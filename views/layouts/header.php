<?php

use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>

                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled"><!-- sidebar-toggled -->
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <!-- end mobile sidebar expand / collapse button -->
                
                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right">
                             <li class="dropdown navbar-user">
                                <a id="btn-dropdown_header2" onclick="$(this).parent().find('ul').slideToggle('fast');" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                    <?=  Yii::$app->session->get('language') ? Yii::$app->session->get('language') : 'ru'; ?><span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/site/change-locale?locale=en">en</a></li>
                                    <li><a href="/site/change-locale?locale=ru">ru</a></li>
                                </ul>
                            </li>
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" onclick="$(this).parent().find('ul').slideToggle('fast');" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                                <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li class="arrow"></li>
                                <li> <?= Html::a('Profile', ['user/profile']) ?> </li>
                                <li class="divider"></li>
                                <li> <?= Html::a('Logout', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>