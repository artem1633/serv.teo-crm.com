<?php

use app\admintheme\widgets\Menu;


?>



<div id="top-menu" class="top-menu">
    <?php if (Yii::$app->user->isGuest == false): ?>        <?php        try {
            echo \app\admintheme\widgets\TopMenu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        // ['label' => Yii::t('app', 'Рабочий стол'), 'icon' => 'fa fa-bar-chart', 'url' => ['/dashboard']],
                        ['label' => Yii::t('app', 'Заявки'), 'icon' => 'fa fa-briefcase', 'url' => ['/order'], 'visible' => Yii::$app->user->identity->can('order_view')],
                        ['label' => Yii::t('app', 'Поставки'), 'icon' => 'fa fa-ambulance', 'url' => ['/deliver'], 'visible' => Yii::$app->user->identity->can('deliver_view')],
                        ['label' => Yii::t('app', 'Справочники'), 'icon' => 'fa fa-list-ul', 'url' => '#', 'options' => ['class' => 'has-sub'], 'visible' => Yii::$app->user->identity->can('books'),
                            'items' => [
    

                                ['label' => Yii::t('app', 'Статус'), 'icon' => 'fa fa-500px', 'url' => ['/status'], ],
                                ['label' => Yii::t('app', 'СЦ'), 'icon' => 'fa fa-adjust', 'url' => ['/service'], ],
                                ['label' => Yii::t('app', 'Контрагент'), 'icon' => 'fa fa-blind', 'url' => ['/client'], ],
                                ['label' => Yii::t('app', 'Тип ремонта'), 'icon' => 'fa fa-align-center', 'url' => ['/type-repair'], ],
                                ['label' => Yii::t('app', 'Номенклатура'), 'icon' => 'fa fa-archive', 'url' => ['/nomenclature'], ],
                                ['label' => Yii::t('app', 'Запчасти'), 'icon' => 'fa fa-ambulance', 'url' => ['/spare'], ],
                        ]],
                            
                    ['label' => Yii::t('app', 'Настройки'), 'icon' => 'fa fa-list-ul', 'url' => '#', 'options' => ['class' => 'has-sub'], 'visible' => true,
                        'items' => [
                                                        ['label' => Yii::t('app', 'Пользователи'), 'icon' => 'fa  fa-user-o', 'url' => ['/user']],
                            ['label' => Yii::t('app', 'Роли'), 'icon' => 'fa  fa-star', 'url' => ['/role']],
                            // ['label' => Yii::t('app', 'Отчеты'), 'icon' => 'fa  fa-star', 'url' => ['/report']],
                            // ['label' => Yii::t('app', 'Поля отчета'), 'icon' => 'fa  fa-star', 'url' => ['/report-column']],
                                                        ['label' => Yii::t('app', 'Шаблоны'), 'icon' => 'fa fa-file-o', 'url' => ['/template']],
                                                    ],
                ],
                ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
