<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Role;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Service;
use app\models\UserService;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if($model->isNewRecord == false){
    $model->services = ArrayHelper::getColumn(UserService::find()->where(['user_id' => $model->id])->all(), 'service_id');
}

?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '+7(999)999-99-99'
    ]) ?>

    <?= $form->field($model, 'role_id')->dropDownList(\yii\helpers\ArrayHelper::map(Role::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'services')->widget(\kartik\select2\Select2::class, [
        'data' => ArrayHelper::map(Service::find()->all(), 'id', 'name'),
        'options' => [
            'multiple' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip']) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
