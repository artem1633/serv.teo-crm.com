<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
	$model->type_work = explode(',', $model->type_work);
}
?>

<style>
    .mfp-wrap{
        z-index: 2000 !important;
    }

    .mfp-bg {
        z-index: 1999 !important;
    }
</style>

<div class="order-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
                                                   <div class="col-md-4" style=" ">                
         <?= $form->field($model, 'type_repair_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TypeRepair::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'asc')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" ">                
         <?= $form->field($model, 'nomenklatura_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Nomenclature::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'sn')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" ">                
         <?= $form->field($model, 'status_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Status::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'komplektaciya')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'vneshniy_vid')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'data_sale')->input('date')  ?>
        </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'create_at')->input('date')  ?>
        </div>
        <div class="col-md-4" style=" ">                
         <?= $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Client::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'repair_status')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" "> <?= $form->field($model, 'type_work')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\TypeRepair::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'tags' => true
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'kontakty')->textInput()  ?>
        </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'fault')->textInput()  ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'done_repair_date')->input('date')  ?>
        </div>
        <div class="col-md-4" style=" ">        
             <?= $form->field($model, 'number_sale')->textInput()  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'comment')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">                
         <?= $form->field($model, 'service_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Service::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'comment_job')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'nomer_otpravleniya')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">                
         <?= $form->field($model, 'zip')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Spare::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-12" style=" ">              
             <?= $form->field($model, 'prichina_vydachi_akta')->textarea()  ?>
        </div>
    </div>

                <div class="col-md-5" style="height: 200px; overflow: auto;">
                      
            <?= \kato\DropZone::widget([
    'id'        => 'dzImage_file',
    'uploadUrl' => \yii\helpers\Url::toRoute([ '/order/upload-file']),
    'dropzoneContainer' => 'dz-container-images',
    'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
    'clientEvents' => [
      'complete' => "function(file){
        var path = JSON.parse(file.xhr.response);
        if($('[name=\'file_file_path\']').val()){
          var value = JSON.parse($('[name=\'file_file_path\']').val());
        } else {
          var value = [];
        }        console.log(value, 'current value');
        value.push(path);
        console.log(value, 'new value');        $('[name=\'file_file_path\']').val(JSON.stringify(value));
        var arr = [ 'jpeg', 'jpg', 'gif', 'png' ];
        var strE = path.url;        var ext = strE.substring(strE.lastIndexOf('.')+1);
        if(arr.includes(ext)){
          strE = path.url;
        } else {
          strE = 'https://shopmalinka.ru/image.php?main=images/product_images/popup_images/000206844_s.jpg';
        }
        var html = '<tr class=\"template-upload fade in\"><td><span class=\"preview\"><img src=\"'+strE+'\" style=\"width: 50px;\"></span></td>';
        html += '<td><p class=\"name\">'+path.name+'</p><p class=\"size\">'+path.size+'</p></td>';
        html += '<td><button data-delete=\"'+path.url+'\" class=\"btn btn-danger cancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i><span>Удалить</span></button></td></tr>';
        $('.files').append(html);
        $('[data-delete]').unbind('click');
        $('[data-delete]').click(deleteEventHandler);
      }",
     ],
]);?>
<div style="display: none;">
  <input type="hidden" name="file_file_path">
</div>
<?php
$script = <<< JS

var deleteEventHandler = function(e){
  e.preventDefault();

  var path = $(this).data('delete');

  var self = $(this).parent().parent();

  if($('[name=\'file_file_path\']').val()){
    var data = JSON.parse($('[name=\'file_file_path\']').val());
  } else {
    var data = [];
  }

  var newData = [];

  console.log(data, 'current data');

  $.each(data, function(){
      if(this.url != path){
        newData.push(this);
      }
  });

  console.log(newData, 'new data');

  $('[name=\'file_file_path\']').val(JSON.stringify(newData));




  $.get("/order/image-delete?id={$model->id}&name="+path, function(){
    $(self).remove();
  });
};

$('[data-delete]').click(deleteEventHandler);


JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
                </div>

                <div class="col-md-7" style="height: 200px; overflow: auto;">
                    <table role="presentation" class="table table-striped">
                        <tbody class="files">
                        <?php if ($model->photo):?>
                        <?php  foreach (json_decode($model->photo, true) as $value):?>
                          <tr class="template-upload fade in">
                                <td>
                                    <span class="preview">
                                      <a class="image-popup-vertical-fit" href="<?php if(exif_imagetype(substr($value['url'], 1)) !== false){echo $value['url'];}else{echo 'https://shopmalinka.ru/image.php?main=images/product_images/popup_images/000206844_s.jpg';}?>"><img src="<?php if(exif_imagetype(substr($value['url'], 1)) !== false){echo $value['url'];}else{echo 'https://shopmalinka.ru/image.php?main=images/product_images/popup_images/000206844_s.jpg';}?>" style="width: 50px;"></a>
                                    </span>
                                </td>
                                <td>
                                    <p class="name"></p><?=$value['name']?></p>
                                    <p class="size"><?=$value['size']?></p>
                                </td>
                                <td>
                                <button data-delete="<?=$value['url']?>" class="btn btn-danger cancel">
                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                    <span>Удалить</span>
                                </button>
                                <a class="btn btn-success" href="<?=$value['url']?>" download="<?=$value['name']?>">Скачать</a>
                                    
                                </td>
                            </tr>
                        <?php endforeach;?>    
                        <?php endif;?>
                      </tbody>
                  </table>
              </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->

<?php

$script = <<< JS

// $("#order-filephoto").change(function(){
//     $('#file-photo-preview')[0].src = URL.createObjectURL($('#order-filephoto')[0].files[0]);
// });

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true,
             markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
              '<a class="img-download-btn" style="display: inline-block; margin-top: 5px; font-size: 15px;"><i class="fa fa-download"></i> Скачать</a>'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>',
        },
         closeOnContentClick: false,
         callbacks: {
            open: function() {
              $('.img-download-btn').attr('href', this.currItem.src);
              $('.img-download-btn').attr('download', this.currItem.el.data('name'));
            },
            close: function() {
              // Will fire when popup is closed
            }
            // e.t.c.
          }
        
    });

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>