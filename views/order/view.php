<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Order */
?>
<div class="row">
    <div class="col-md-12">
        <p>
            <?= \yii\helpers\Html::a('<i class="fa fa-angle-double-left"></i> Назад', '#', ['class' => 'btn btn-white', 'onclick' => 'event.preventDefault(); window.history.go(-1); return false;']) ?>
        </p>
    </div>
</div>
<div class="order-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a class="btn btn-warning btn-xs" href="/order/update?id=<?= $model->id?>&amp;containerPjaxReload=%23pjax-container-info-container" role="modal-remote"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'attribute' => 'type_repair_id',
                                        'value' => function($model){
                                            return \yii\helpers\ArrayHelper::getValue($model, 'typeRepair.name');
                                        }
                                    ],
            'asc',
            'nomenklatura_id',
            'sn',
            'status_id',
            'komplektaciya',
            'vneshniy_vid',
            'data_sale',
            'create_at',
            'client_id',
            'repair_status',
            'sum',
            'injener_id',
            'kontakty',
            'fault',
            'photo',
            'user_id',
            'number_sale',
            'adress_del',
            'comment',
            'service_id',
            'comment_job',
            'nomer_otpravleniya',
            'zip',
            'prichina_vydachi_akta',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/job/index.php", [
            'searchModel' => $jobSearchModel,
            'dataProvider' => $jobDataProvider,
            'additionalLinkParams' => ['Job[order_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
