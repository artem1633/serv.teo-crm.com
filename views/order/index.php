<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $searchModel OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Заявки";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(isset($additionalLinkParams)){
    $createUrl = ArrayHelper::merge(['order/create'], $additionalLinkParams);
    $createUrl = ArrayHelper::merge($createUrl, ['display' => false]);
} else {
    $createUrl = ['order/create'];
}

?>
<style>
    .modal-dialog {
        width: 80% !important;
    }
</style>


<style>
    #ajaxCrudDatatable .panel-info>.panel-heading {
        display: none!important;
    }
    #ajaxCrudDatatable .panel-info>.kv-panel-before>.pull-right {
        float: left!important;
    }
    #ajaxCrudDatatable .panel-info>.table-responsive {
        padding:  0px !important;
    }
</style>
<div class="panel panel-inverse">
   <div class="panel-body">
       <div class="row">
           <div class="col-md-12">
               <?= Html::a('Все не завершенные', ['not-fin'], ['class' => Yii::$app->controller->action->id == 'not-fin' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Принятые в ремонт', ['ap-in-job'], ['class' => Yii::$app->controller->action->id == 'ap-in-job' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('В ремонт', ['in-job'], ['class' => Yii::$app->controller->action->id == 'in-job' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Ожидание ЗИП', ['zip'], ['class' => Yii::$app->controller->action->id == 'zip' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Ожидание ЗИП (Отправлено)', ['zip-out'], ['class' => Yii::$app->controller->action->id == 'zip-out' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Ожидают утверждения', ['approv'], ['class' => Yii::$app->controller->action->id == 'approv' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Отклоненные', ['cancel'], ['class' => Yii::$app->controller->action->id == 'cancel' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Завершенные', ['fin'], ['class' => Yii::$app->controller->action->id == 'fin' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Удаленные', ['del'], ['class' => Yii::$app->controller->action->id == 'del' ? 'btn btn-success' : 'btn btn-default']) ?>
               <?= Html::a('Все ремонты', ['all'], ['class' => Yii::$app->controller->action->id == 'all' ? 'btn btn-success' : 'btn btn-default']) ?>
           </div>
           <div class="col-md-2">
           </div>
       </div>
   </div>
</div>
<div class="panel panel-inverse project-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Заявки</h4>
    </div>
    <div class="panel-body">
        <div class="order-index">
            <div id="ajaxCrudDatatable">
               <?php \yii\widgets\Pjax::begin(['id' => 'crud-datatable-order-pjax']); ?>
                <?php $dynagrid = DynaGrid::begin([
                    'storage'=>DynaGrid::TYPE_COOKIE,
                    'showPersonalize'=>true,
                    'columns' => require (__DIR__.'/_columns.php'),
                    'theme'=>'panel-info',
                    'options' => [
                        'id' => "crud-datatable-order-".Yii::$app->controller->action->id,
                    ],
                    'allowSortSetting' => false,
                    'allowPageSetting' => true,
                    'allowFilterSetting' => false,
                    'allowThemeSetting' => false,
                    'gridOptions'=>[
                        'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
                        'panel'=>[
                            'heading'=>null,
                            'before' =>  '<div style="padding-top: 7px;"><em></em></div>',
                            'after' => false
                        ],
                        'rowOptions' => function($model){
                          return ['class' => 'danger'];
                        },
                        'showPageSummary'=>true,
                        'floatHeader'=>false,
                        'pjax'=>false,
                        'responsiveWrap'=>false,
                        'containerOptions' => [
                            'style' => 'padding: 20px;',
                        ],'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Are you sure?',
                                            'data-confirm-message'=>'Вы действительно хотите удалить эту запись?'
                                        ]).' <div style="display: none;">'.Html::a('<span class="glyphicon glyphicon-print"></span> Распечатать', ['bulk-print'], ['id' => 'bulk-print-btn', 'class' => 'btn btn-info btn-xs', 'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post', 'title' => 'Печать']).'</div>',
                                ]).                        
                                '<div class="clearfix"></div>',
                    ],
                        'toolbar' =>  [
                            ['content'=>
                                Html::a('<i class="fa fa-plus"></i>', $createUrl, ['type'=>'button', 'title'=>'Добавить', 'class'=>'btn btn-success', 'role' => 'modal-remote']) . ' '.                                '<button type="button" class="btn btn-danger" title="Персонализировать настройки таблицы" onclick="$(\'#crud-datatable-order-'.Yii::$app->controller->action->id.'-grid-modal\').modal(\'show\');" ><i class="glyphicon glyphicon-wrench"></i></button>'. '     '.Html::a('Импорт', ['add'],
                            ['role'=>'modal-remote','title'=> 'Импорт', 'class'=>'btn btn-warning'])   . Html::a('Скачать акт', \yii\helpers\ArrayHelper::merge(['order/excel'], Yii::$app->request->queryParams), ['class' => 'btn btn-info', 'data-pjax' => 0])   .Html::a('<span class="glyphicon glyphicon-print"></span> Распечатать', '#', ['class' => 'btn btn-info', 'onclick' => 'event.preventDefault(); $("#bulk-print-btn").trigger("click");'])                          ],
                            ['content'=>'<div style="display: none;">{dynagridFilter}{dynagridSort}{dynagrid}</div>'],
                            '{export}',
                        ]
                    ],
                ]);

                 DynaGrid::end() ?>
            <?php \yii\widgets\Pjax::end(); ?>
            </div>

        </div>
    </div>
</div>


            
<?php

$pks = [];


foreach ($allDataProvider->models as $model) {
  // if($model->done_repair_date == date('Y-m-d', time() + 86400)){
    // $pks[] = $model->id;
  // }


  $now = time();
  $your_date = strtotime($model->create_at);
  $datediff = $now - $your_date;

  if($model->create_at == null){
    continue;
  }

  $days = floor($datediff / (60 * 60 * 24));

  if($model->client_id){
    if($days >= 30){
      $pks[] = $model->id;
    }
  } elseif($model->client_id == null && $model->type_repair_id == 3) {
    if($days >= 30){
      $pks[] = $model->id;
    }
  }


}


$pks = json_encode($pks);

$script = <<< JS

var pks = {$pks};

    jQuery(document).on("pjax:success", "#crud-datatable-order-pjax",  function(event){
              for(i = 0; i < pks.length; i++)
              {
                var id = pks[i];
                $('[data-key="'+id+'"]').addClass("warning");
              }

              $('[data-key]').click(function(e){
                  if($(e.target).is('td')){
                      var id = $(this).data('key');
                      window.location = '/order/view?id='+id;
                  }
              });

          }
        );

for(i = 0; i < pks.length; i++)
{
  var id = pks[i];
  $('[data-key="'+id+'"]').addClass("warning");
}

$('[data-key]').click(function(e){
    if($(e.target).is('td')){
        var id = $(this).data('key');
        window.location = '/order/view?id='+id;
    }
});

$(document).ready(function(){
    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true,
             markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>',
        },
         closeOnContentClick: false,
        
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
