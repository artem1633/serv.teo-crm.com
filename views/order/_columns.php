<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    // [
    //     'class' => 'kartik\grid\ActionColumn',
    //     'dropdown' => false,
    //     'vAlign'=>'middle',
    //     'urlCreator' => function($action, $model, $key, $index) { 
    //             return Url::to(["order"."/".$action,'id'=>$key]);
    //     },
    //     'template' => '{view}',
    //     'buttons' => [
    //         'print' => function($url, $model){
    //             return Html::a('<span class="glyphicon glyphicon-print"></span>', ['print', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Печать']);
    //         },
    //     ],
    //     'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
    //     'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
    //     'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
    //                       'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
    //                       'data-request-method'=>'post',
    //                       'data-toggle'=>'tooltip',
    //                       'data-confirm-title'=>'Вы уверены?',
    //                       'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    // ],
        [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type_repair_id',
        'visible'=>1,
        'value'=>'typeRepair.name',
        'filter'=> ArrayHelper::map(\app\models\TypeRepair::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'asc',
         'content' => function($model){
         	return \yii\helpers\Html::a($model->asc, ['order/update', 'id' => $model->id], ['role' => 'modal-remote']);
         },
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nomenklatura_id',
        'visible'=>1,
        'value'=>'nomenklatura.name',
        'filter'=> ArrayHelper::map(\app\models\Nomenclature::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'sn',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'visible'=>1,
        'value'=>'status.name',
        'filter'=> ArrayHelper::map(\app\models\Status::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'komplektaciya',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'vneshniy_vid',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_sale',
        'visible'=>1,
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#ordersearch-order_date").val(""); $("#ordersearch-order_date").trigger("change"); }'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'convertFormat'=>true,
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'YYYY-MM-DD'
                ]
            ],
        ],
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'create_at',
        'visible'=>1,
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginEvents' => [
                'cancel.daterangepicker'=>'function(ev, picker) {$("#ordersearch-order_date").val(""); $("#ordersearch-order_date").trigger("change"); }'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'convertFormat'=>true,
                'locale' => [
                    'cancelLabel' => 'Clear',
                    'format' => 'YYYY-MM-DD'
                ]
            ],
        ],
        'format'=> ['date', 'php:d.m.Y'],

    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'visible'=>1,
        'value'=>'client.name',
        'filter'=> ArrayHelper::map(\app\models\Client::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'repair_status',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'type_work',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'sum',
        'visible' => 0,
        'format' =>['decimal', 2],
        'pageSummary' => true, 
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'injener_id',
        'visible'=>1,
        'value'=>'injener.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kontakty',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'fault',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'photo',
         'content' => function($model){
         	// if($model->photo){

         	$type = null;
         	if($model->photo){
         		$photo = explode('.', $model->photo);
         		if(count($photo) >= 2){
         			if(in_array($photo[1], ['png', 'jpg']) == false){
         				return "<img src='/img/no-photo.png' style='object-fit: cover; heigth: 150px; width: 100px; border: 1px solid #cecece; border-radius: 10px;'>";
         			}
         		}
         	}

         	return "<a class='image-popup-vertical-fit' href='/".($model->photo ? $model->photo : 'img/no-photo.png')."'><img src='/".($model->photo ? $model->photo : 'img/no-photo.png')."' style='object-fit: cover; heigth: 150px; width: 100px; border: 1px solid #cecece; border-radius: 10px;'>";
         	// }
         }, 
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'label'=>'Кол-во дней',
         'content' => function($model){
         	// if($model->photo){

			$now = time(); // текущее время (метка времени)
			$your_date = strtotime($model->create_at); // какая-то дата в строке (1 января 2017 года)
			$datediff = $now - $your_date; // получим разность дат (в секундах)

			return floor($datediff / (60 * 60 * 24));
         	// }
         }, 
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'visible'=>0,
        'value'=>'user.name',
        'filter'=> ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'number_sale',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'adress_del',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'service_id',
        'visible'=>1,
        'value'=>'service.name',
        'filter'=> ArrayHelper::map(\app\models\Service::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment_job',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'nomer_otpravleniya',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'zavodskoy_nomer',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'zip',
        'visible'=>1,
        'value'=>'zip.name',
        'filter'=> ArrayHelper::map(\app\models\Spare::find()->asArray()->all(), 'id', 'name'),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => '', 'multiple' => true],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'prichina_vydachi_akta',
         'visible'=>0,
    ],

];   