<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
        'label' => 'Номер',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'asc',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nomenklatura_id',
        'value' => 'nomenklatura.name',
        'label' => 'Аппарат',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'sn',
         'label' => 'Серийный номер',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'label' => 'Дата поступления в СЦ',
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment_job',
         'label' => 'Выполненная работа',
    ],
];   