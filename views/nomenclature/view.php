<?php

use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model Nomenclature */
?>
<div class="nomenclature-view">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a class="btn btn-warning btn-xs" href="/nomenclature/update?id=<?= $model->id?>&amp;containerPjaxReload=%23pjax-container-info-container" role="modal-remote"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <div class="row">
                        <div class="col-md-12">

                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'name',
            'code',
                                ],
                            ]) ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    




    
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/order/index.php", [
            'searchModel' => $orderSearchModel,
            'dataProvider' => $orderDataProvider,
            'additionalLinkParams' => ['Order[nomenklatura_id]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/spare/index.php", [
            'searchModel' => $spareSearchModel,
            'dataProvider' => $spareDataProvider,
            'additionalLinkParams' => ['Spare[nomenklatura]' => $model->id],
        ]); ?>
    </div>
            
        
    <div class="col-md-12">
   <?= $this->render("@app/views/deliver/index.php", [
            'searchModel' => $deliverSearchModel,
            'dataProvider' => $deliverDataProvider,
            'additionalLinkParams' => ['Deliver[nomenklatura_id]' => $model->id],
        ]); ?>
    </div>
    

      </div>

    
</div>
