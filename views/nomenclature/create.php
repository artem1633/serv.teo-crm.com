<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Nomenclature */

?>
<div class="nomenclature-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
