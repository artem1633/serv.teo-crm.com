<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Deliver */
?>
<div class="deliver-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
