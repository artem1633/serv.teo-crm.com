<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Deliver */

?>
<div class="deliver-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
