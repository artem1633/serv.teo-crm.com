<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Deliver */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>

<div class="deliver-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                  <div class="col-md-8" style=" ">                
         <?= $form->field($model, 'nomenklatura_id')->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\models\Nomenclature::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите'
                    ],
                ]) ?>
    </div>
        <div class="col-md-4" style=" ">      
             <?= $form->field($model, 'origin')->checkbox()  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'count')->textInput(['type' => 'number'])  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'sn')->textInput()  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'meriynik')->textInput()  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'garantiya')->textInput(['type' => 'number'])  ?>
        </div>
        <div class="col-md-6" style=" ">        
             <?= $form->field($model, 'partiya')->textInput()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->
