<?php
use app\models\Template;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'type')->dropDownList(Template::typeLabels()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'text')->widget(\mihaildev\ckeditor\CKEditor::class, []) ?>
        </div>
    </div>

    <div id="example-template">
		<p>Заявки: Тип (<b>{тип}</b>), Заказ наряд (<b>{заказ-наряд}</b>), Номенклатура (<b>{номенклатура}</b>), Серийный номер (<b>{серийный-номер}</b>), Комплектация (<b>{комплектация}</b>), Внешний вид (<b>{внешний-вид}</b>), Дата продажи (<b>{дата-продажи}</b>), Дата приема (<b>{дата-приема}</b>), Клиент (<b>{клиент}</b>), Состояние ремонта (<b>{состояние-ремонта}</b>), Тип работы (<b>{тип-работы}</b>), Стоимость ремонта (<b>{стоимость-ремонта}</b>), Комментарий к выполненной работе (<b>{комментарий-к-выполненной-работе}</b>), Инженер (<b>{инженер}</b>), Контакты (<b>{контакты}</b>), Неисправность (<b>{неисправность}</b>), СЦ (<b>{сц}</b>), Фото (<b>{фото}</b>), Пользователь (<b>{пользователь}</b>), Заказ-наряд продавца (<b>{заказ-наряд-продавца}</b>), Адрес доставки (<b>{адрес-доставки}</b>), Статус (<b>{статус}</b>), Комментарий (<b>{комментарий}</b>), Номер отправления (<b>{номер-отправления}</b>), ЗИП (<b>{зип}</b>), заводской номер (<b>{заводской-номер}</b>), Причина выдачи акта (<b>{причина-выдачи-акта}</b>)</p>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
