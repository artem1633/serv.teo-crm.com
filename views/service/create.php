<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Service */

?>
<div class="service-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
