<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Spare */
?>
<div class="spare-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
