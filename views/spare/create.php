<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Spare */

?>
<div class="spare-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
