<?php

use yii\helpers\Html;

/** @var $templates \app\models\Template */
/** @var $id int */

?>

<style>
	@media print {
  .pagebreak { 
     page-break-before: always; 
  }
}

@media print {
    .pagebreak {
        clear: both;
        page-break-after: always;
    }
}
</style>

<?php foreach ($templates as $template): ?>

    <?= Html::a($template->name, ['print-template', 'pks' => $pks, 'template_id' => $template->id], ['class' => 'btn btn-success btn-block', 'target' => '_blank']); ?>

<?php endforeach; ?>
