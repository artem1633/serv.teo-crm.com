<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model TypeRepair */
?>
<div class="type-repair-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
