<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model TypeRepair */

?>
<div class="type-repair-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
