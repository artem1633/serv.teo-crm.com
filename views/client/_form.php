<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                      <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
         <?= $form->field($model, 'type')->dropDownList(app\models\Client::typeLabels()) ?>
      
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'yr_adress')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'kr_adress')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'pos_adress')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'all_phone')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'email')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'manag')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'manag_phone')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'manag_email')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'inn')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'bik')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'schet')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'kor_schet')->textInput()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->
