<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model Client */

?>
<div class="client-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
