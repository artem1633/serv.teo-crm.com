<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(["client"."/".$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить эту позицию'], 
    ],
        [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'name',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'visible'=>1,
        'filter'=> \app\models\Client::typeLabels(),
        'filterType'=> GridView::FILTER_SELECT2,
        'filterWidgetOptions'=> [
               'options' => ['prompt' => ''],
               'pluginOptions' => [
                      'allowClear' => true,
                      'tags' => false,
                      'tokenSeparators' => [','],
               ]
        ]
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'yr_adress',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kr_adress',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'pos_adress',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'all_phone',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'email',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manag',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manag_phone',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manag_email',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'inn',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'bik',
         'visible'=>1,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'schet',
         'visible'=>0,
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'kor_schet',
         'visible'=>0,
    ],

];   