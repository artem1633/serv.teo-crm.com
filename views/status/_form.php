<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Status */
/* @var $form yii\widgets\ActiveForm */

$displayNone = '';
if (isset($_GET['display'])){
    $displayNone = 'display:none;';
}
if($model->isNewRecord == false){
}
?>

<div class="status-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
                                 <div class="col-md-12" style=" ">        
             <?= $form->field($model, 'name')->textInput()  ?>
        </div>
        <div class="col-md-12" style=" ">      
             <?= $form->field($model, 'info_admin')->checkbox()  ?>
        </div>
        <div class="col-md-12" style=" ">      
             <?= $form->field($model, 'info_master')->checkbox()  ?>
        </div>
        <div class="col-md-12" style=" ">      
             <?= $form->field($model, 'info_client')->checkbox()  ?>
        </div>
        <div class="col-md-12" style=" ">              
             <?= $form->field($model, 'text_admin')->textarea()  ?>
        </div>
        <div class="col-md-12" style=" ">              
             <?= $form->field($model, 'text_master')->textarea()  ?>
        </div>
        <div class="col-md-12" style=" ">              
             <?= $form->field($model, 'text_client')->textarea()  ?>
        </div>
        <div class="col-md-12" style=" ">      
             <?= $form->field($model, 'is_archive')->checkbox()  ?>
        </div>
        <div class="col-md-12" style=" ">      
             <?= $form->field($model, 'is_write')->checkbox()  ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<!-- <script src="/libs/jquery.maskedinput.min.js"></script> -->

<!-- <script>$("#client-phone").mask("+7 (999) 999-9999");</script>-->
