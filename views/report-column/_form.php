<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="row">
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Заявки</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'order_type_repair_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_asc')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_nomenklatura_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_sn')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_status_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_komplektaciya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_vneshniy_vid')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_data_sale')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_client_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_repair_status')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_type_work')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_sum')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_injener_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_kontakty')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_fault')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_photo')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_user_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_number_sale')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_adress_del')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_comment')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_service_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_comment_job')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_nomer_otpravleniya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_zavodskoy_nomer')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_zip')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'order_prichina_vydachi_akta')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Работы</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'job_typ_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'job_sum')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'job_replace_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'job_spare_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'job_spare_old_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'job_order_id')->checkbox() ?>
                </div>

            </div>
        </div>
            <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="color:#16508A;">Поставки</h5>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_nomenklatura_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_origin')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_count')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_sn')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_meriynik')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_garantiya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_partiya')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_create_at')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_who_id')->checkbox() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'deliver_sc_id')->checkbox() ?>
                </div>

            </div>
        </div>
    </div>

    <?php  if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php  } ?>

    <?php  ActiveForm::end(); ?>
</div>
