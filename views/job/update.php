<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Job */
?>
<div class="job-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
