Регистрация пользователя<br>
api/user/register<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
-email* (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
<br>
Изменить пользователя<br>
api/user/edit<br>
[<span style="color: red">POST</span>]<br>
-name (string)<br>
-email (string)<br>
ОТДАЕТ: Объект<br>
<br>
Установить FCM-токен<br>
api/user/set-fcm-token<br>
[<span style="color: red">POST</span>]<br>
-fcm_token* (string)<br>
{result: true}<br>
<br>
Получение токена<br>
api/user/get-token<br>
[<span style="color: red">POST</span>]<br>
-login* (string)<br>
-password* (string)<br>
ОТДАЕТ: токен для авторизации (string)<br>
Отправить заявку на востановление пароля<br>
<br>
Просмотр Тонер<br>
api/toner/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Toner, Toner, ...]<br>
<br>
Создание Тонер<br>
api/toner/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Тонер<br>
api/toner/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Тонер<br>
api/toner/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Тонер<br>
api/toner/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Фотобарабан<br>
api/photoconductor/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Photoconductor, Photoconductor, ...]<br>
<br>
Создание Фотобарабан<br>
api/photoconductor/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Фотобарабан<br>
api/photoconductor/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Фотобарабан<br>
api/photoconductor/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Фотобарабан<br>
api/photoconductor/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Работы<br>
api/job/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Job, Job, ...]<br>
<br>
Создание Работы<br>
api/job/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Работы<br>
api/job/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Работы<br>
api/job/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Работы<br>
api/job/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Картридж<br>
api/cartridge/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Cartridge, Cartridge, ...]<br>
<br>
Создание Картридж<br>
api/cartridge/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Картридж<br>
api/cartridge/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Картридж<br>
api/cartridge/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Картридж<br>
api/cartridge/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр клиента<br>
api/customer/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Customer, Customer, ...]<br>
<br>
Создание клиента<br>
api/customer/create<br>
[<span style="color: red;">POST</span>]<br>
-name (Текст) Наименование<br>
-bin (Текст) Бин<br>
-telephone (Текст) Телефон<br>
-contract (Текст) Договор<br>
-payment (Текст) Расчет<br>
-valid_to (Дата и время) Действителен до<br>
-black_list (Текст) Черный список<br>
-comment (Текст) Комментарий<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить клиента<br>
api/customer/update<br>
[<span style="color: red;">POST</span>]<br>
-name (Текст) Наименование<br>
-bin (Текст) Бин<br>
-telephone (Текст) Телефон<br>
-contract (Текст) Договор<br>
-payment (Текст) Расчет<br>
-valid_to (Дата и время) Действителен до<br>
-black_list (Текст) Черный список<br>
-comment (Текст) Комментарий<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр клиента<br>
api/customer/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить клиента<br>
api/customer/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр заявок<br>
api/applications/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Applications, Applications, ...]<br>
<br>
Создание заявок<br>
api/applications/create<br>
[<span style="color: red;">POST</span>]<br>
-status (Текст) Статус<br>
-customer_id (Текст) Клиент<br>
-full_name* (Текст) ФИО<br>
-request (Текст) Заявка<br>
-date_of_adoption (Дата и время) Дата принятия<br>
-date_of_completion (Дата и время) Дата выполнения<br>
-date_of_payment (Дата и время) Дата оплаты<br>
-accepted (Текст) Принял<br>
-fulfilled (Текст) Выполнил<br>
-comment (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-telephone (Текст) Телефон<br>
-material_price (Текст) Цена материала<br>
-price_of_work (Текст) Цена работы<br>
-file (Файл) Файл<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить заявку<br>
api/applications/update<br>
[<span style="color: red;">POST</span>]<br>
-status (Текст) Статус<br>
-customer_id (Текст) Клиент<br>
-full_name* (Текст) ФИО<br>
-request (Текст) Заявка<br>
-date_of_adoption (Дата и время) Дата принятия<br>
-date_of_completion (Дата и время) Дата выполнения<br>
-date_of_payment (Дата и время) Дата оплаты<br>
-accepted (Текст) Принял<br>
-fulfilled (Текст) Выполнил<br>
-comment (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-telephone (Текст) Телефон<br>
-material_price (Текст) Цена материала<br>
-price_of_work (Текст) Цена работы<br>
-file (Файл) Файл<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр заявок<br>
api/applications/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить заявку<br>
api/applications/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр картриджа<br>
api/cartridges/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Cartridges, Cartridges, ...]<br>
<br>
Создание картриджа<br>
api/cartridges/create<br>
[<span style="color: red;">POST</span>]<br>
-customer (Текст) Клиент<br>
-accepted (Текст) Принят<br>
-completed (Текст) Выполнен<br>
-given_away (Текст) Отдан<br>
-job_id (Связь со справочником) Работа<br>
-full_name (Текст) ФИО<br>
-cartridge_id (Связь со справочником) Картридж<br>
-fulfilled (Текст) Выполнил<br>
-comments (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-expenses (Текст) Затраты<br>
-status (Текст) Статус<br>
-toner_id (Связь со справочником) Тонер<br>
-toner_quantity (Текст) Количество тонера<br>
-drum_id (Связь со справочником) Фотобарабан<br>
-number_of_imaging_drums (Текст) Количество фотобарабанов<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить картридж<br>
api/cartridges/update<br>
[<span style="color: red;">POST</span>]<br>
-customer (Текст) Клиент<br>
-accepted (Текст) Принят<br>
-completed (Текст) Выполнен<br>
-given_away (Текст) Отдан<br>
-job_id (Связь со справочником) Работа<br>
-full_name (Текст) ФИО<br>
-cartridge_id (Связь со справочником) Картридж<br>
-fulfilled (Текст) Выполнил<br>
-comments (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-expenses (Текст) Затраты<br>
-status (Текст) Статус<br>
-toner_id (Связь со справочником) Тонер<br>
-toner_quantity (Текст) Количество тонера<br>
-drum_id (Связь со справочником) Фотобарабан<br>
-number_of_imaging_drums (Текст) Количество фотобарабанов<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр картриджа<br>
api/cartridges/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить картридж<br>
api/cartridges/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр техники<br>
api/technics/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Technics, Technics, ...]<br>
<br>
Создание техники<br>
api/technics/create<br>
[<span style="color: red;">POST</span>]<br>
-customer (Текст) Клиент<br>
-full_name (Текст) ФИО<br>
-accepted (Дата и время) Принят<br>
-telephone (Текст) Телефон<br>
-technics (Текст) Техника<br>
-numbers (Текст) Номера<br>
-problems (Текст) Проблема<br>
-executor (Текст) Исполнитель<br>
-comment (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-status (Текст) Статус<br>
-material_price (Текст) Цена материала<br>
-price_of_work (Текст) Цена работы<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить технику<br>
api/technics/update<br>
[<span style="color: red;">POST</span>]<br>
-customer (Текст) Клиент<br>
-full_name (Текст) ФИО<br>
-accepted (Дата и время) Принят<br>
-telephone (Текст) Телефон<br>
-technics (Текст) Техника<br>
-numbers (Текст) Номера<br>
-problems (Текст) Проблема<br>
-executor (Текст) Исполнитель<br>
-comment (Текст) Комментарий<br>
-sum (Текст) Сумма<br>
-payment (Текст) Расчет<br>
-status (Текст) Статус<br>
-material_price (Текст) Цена материала<br>
-price_of_work (Текст) Цена работы<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр техники<br>
api/technics/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить технику<br>
api/technics/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
