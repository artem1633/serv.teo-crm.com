<?php

namespace app\modules\api\controllers;

use app\models\forms\UploadFileForm;
use app\models\MobileUserFile;
use Yii;
use app\models\MobileUser;
use app\models\PasswordForget;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class UserController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new RegisterModel();

        if($model->load($data, 'model')) {

            $token = $model->register();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->fcm_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }


    /**
     * POST
     */
    public function actionChangePassword()
    {
        $user = MobileUser::find()->where(['password' => $_POST['oldPassword']])->andWhere(['id' => $this->user->id])->one();

        if($user == null){
            return ['error' => 'Неверный пароль'];
        }

        $user->password = $_POST['newPassword'];
        $result = $user->save(false);

        return ['result' => $result];
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->user;

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }


    /**
     * POST
     */
    public function actionGetToken()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Login();

        if($model->load($data, 'model')) {

            $token = $model->login();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    public function beforeAction($action)
    {
        if(in_array($action->id, ['change-password', 'set-fcm-token', 'edit', 'upload-photo'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = MobileUser::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }
}