<?php 
namespace app\modules\mobile\controllers;

use app\models\Company;
use app\models\MobileUser;
use app\models\Job;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use app\models\User;
use app\behaviors\RoleBehavior;

class JobController extends Controller
{
    public $rawData;

    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Job::find(),
                'actions' => [
                    'create' => 'job_create',
                    'update' => 'job_update',
                    'view' => 'job_view',
                    'delete' => 'job_delete',
                    'bulk-delete' => 'job_delete',
                    'index' => ['job_view', 'job_view_all'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new \app\models\JobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $page = isset($_GET['p']) ? $_GET['p'] : 0;

        $dataProvider->pagination->page = $page;

        foreach($_GET as $key => $value){
            if($key == 'token' || $key == 'p'){
                continue;
            }

            $dataProvider->query->andFilterWhere(['like', $key, $value]);
        }

        $models = $dataProvider->models;
        $result["action_left"] = [
            [
                "url" => "/update",
                "icon" => "fa fa-pencil",
                "color" => "#4B78BC"
            ],
            [
                "url" => "/delete",
                "icon" => "fa fa-trash",
                "color" => "#BC4B4B"
            ],

        ];
        $result["action_right"] = [
            [
                "url" => "/view",
                "icon" => "fa fa-eye",
                "color" => "#54BC4B"
            ],
        ];
        
        $result["form"] = [
   
            [
                "name" => 'typ_id',
                'label' => Yii::t('app', 'Тип'),
                "type" => "books",
                "filter" => "name",
                "url" => "type-repair/index"
            ],      
            [
                "name" => 'sum',
                'label' => Yii::t('app', 'Стоимость'),
                "type" => "number"
            ],               [
                "name" => 'replace_id',
                'label' => Yii::t('app', 'Заменено'),
                "type" => "input"
            ],   
            [
                "name" => 'spare_id',
                'label' => Yii::t('app', 'Запчасть остается'),
                "type" => "books",
                "filter" => "name",
                "url" => "spare/index"
            ],      
            [
                "name" => 'spare_old_id',
                'label' => Yii::t('app', 'Запчасть утиль'),
                "type" => "books",
                "filter" => "name",
                "url" => "spare/index"
            ],      
            [
                "name" => 'order_id',
                'label' => Yii::t('app', 'Заявка'),
                "type" => "books",
                "filter" => "asc",
                "url" => "order/index"
            ],               
        ];
    
        array_walk($models, function(&$model){
            if ($model->typ_id) $model->typ_id = $model->typ->name;
            if ($model->spare_id) $model->spare_id = $model->spare->name;
            if ($model->spare_old_id) $model->spare_old_id = $model->spareOld->name;
            if ($model->order_id) $model->order_id = $model->order->asc;
            
        });
        $result["data"] = $models;
        return $result;
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $this->rawData];
        $model = new Job();

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return ["error" => $model->errors];
        }

        return ['result' => false];
    }

    /**
    * @return mixed
    */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $data = ['model' => $this->rawData];
        $model = Job::findOne($id);

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return ["error" => $model->errors];
        }

        return ['result' => false];
    }

    public function actionView($id)
    {
        $model = Job::findOne($id);

        $result["form"] = [            [
                "name" => 'typ_id',
                'label' => Yii::t('app', 'Тип'),
                "type" => "input"
            ],   
            [
                "name" => 'sum',
                'label' => Yii::t('app', 'Стоимость'),
                "type" => "number"
            ],                      [
                "name" => 'replace_id',
                'label' => Yii::t('app', 'Заменено'),
                "type" => "input"
            ],            [
                "name" => 'spare_id',
                'label' => Yii::t('app', 'Запчасть остается'),
                "type" => "input"
            ],            [
                "name" => 'spare_old_id',
                'label' => Yii::t('app', 'Запчасть утиль'),
                "type" => "input"
            ],            [
                "name" => 'order_id',
                'label' => Yii::t('app', 'Заявка'),
                "type" => "input"
            ],            
        ];
        
        $result["mini_menu"] = [
            'items' => [
                   
            
        ]];
        $result["data"] = $model;
        return $result;
    }

    public function actionDelete($id)
    {
        $model = Job::findOne($id);

        if($model){
            $model->delete();
        }

        return ['success' => true];
    }

    public function actionFile()
    {
        $file = \yii\web\UploadedFile::getInstanceByName("file");

        if($file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$file->extension;

            $file->saveAs($path);

            return ['link' => $path];
        }

        return ['link' => null];
    }

    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;

        $content = file_get_contents('php://input');
        $this->rawData = json_decode($content, true);

        if($action->id != 'login')
        {
            $token = null;
            if(isset($_GET['token'])){
                $token = $_GET['token'];
            } elseif(isset($this->rawData['token'])){
                $token = $this->rawData['token'];
            }

            if($token == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Токен не указан');
            }

            $token = explode('||', $token);

            $user = \app\models\User::find()->where(['login' => $token[0], 'password_hash' => $token[1]])->one();

            if($user == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Неверный логин или пароль');
            }

            Yii::$app->user->login($user, 0);
        }

        return parent::beforeAction($action);
    }
}