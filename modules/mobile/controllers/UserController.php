<?php

namespace app\modules\mobile\controllers;

use app\models\forms\UploadFileForm;
use app\models\MobileUserFile;
use Yii;
use app\models\MobileUser;
use app\models\PasswordForget;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use app\models\User;

class UserController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    private $rawData;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new RegisterModel();

        if($model->load($data, 'model')) {

            $token = $model->register();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->fcm_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }


    /**
     * POST
     */
    public function actionChangePassword()
    {
        $user = MobileUser::find()->where(['password' => $_POST['oldPassword']])->andWhere(['id' => $this->user->id])->one();

        if($user == null){
            return ['error' => 'Неверный пароль'];
        }

        $user->password = $_POST['newPassword'];
        $result = $user->save(false);

        return ['result' => $result];
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->user;

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
    * @return mixed
    */
    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $this->rawData];
        $model = User::findOne(Yii::$app->user->getId());

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return ["error" => $model->errors];
        }

        return ['result' => false];
    }

    public function actionView()
    {
        $model = User::findOne(Yii::$app->user->identity->id);

        $result["lang"] = 
               [ "name" => 'lang',
                'label' => Yii::t('app', 'Язык'),
                "type" => "dropdown",
                "data" => [
                    ["label" => "Английский", "value" => "en",],
                    ["label" => "Русский", "value" => "ru",],
  
                ]
            ];

        $result["form"] = [            
            [
                "name" => 'login',
                'label' => $model->getAttributeLabel('login'),
                "type" => "input"
            ],    
            [
                "name" => 'password',
                'label' => $model->getAttributeLabel('password'),
                "type" => "input"
            ],            
            [
                "name" => 'role_id',
                'label' => $model->getAttributeLabel('role_id'),
                "type" => "books",
                "filter" => "name",
                "url" => "role/index"
            ],   
            [
                "name" => 'role',
                'label' => $model->getAttributeLabel('role'),
                "type" => "input",
            ],
            [
                "name" => 'name',
                'label' => $model->getAttributeLabel('name'),
                "type" => "input"
            ],   
            [
                "name" => 'phone',
                'label' => $model->getAttributeLabel('phone'),
                "type" => "text"
            ],      
        ];
        

        $result["data"] = $model;

        return $result;
    }

    /**
     * POST
     */
    public function actionGetToken()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Login();

        if($model->load($data, 'model')) {

            $token = $model->login();

            if($token == null){
                return $model->errors;
            }

            return ['result' => $token];
        }

        return ['result' => false];
    }

    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;

        $content = file_get_contents('php://input');
        $this->rawData = json_decode($content, true);

        if($action->id != 'login')
        {
            $token = null;
            if(isset($_GET['token'])){
                $token = $_GET['token'];
            } elseif(isset($this->rawData['token'])){
                $token = $this->rawData['token'];
            }

            if($token == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Токен не указан');
            }

            $token = explode('||', $token);

            $user = \app\models\User::find()->where(['login' => $token[0], 'password_hash' => $token[1]])->one();

            if($user == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Неверный логин или пароль');
            }

            Yii::$app->user->login($user, 0);
        }

        return parent::beforeAction($action);
    }
}