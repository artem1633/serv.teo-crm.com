<?php 
namespace app\modules\mobile\controllers;

use app\models\Company;
use app\models\MobileUser;
use app\models\Order;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use app\models\User;
use app\behaviors\RoleBehavior;

class OrderController extends Controller
{
    public $rawData;

    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Order::find(),
                'actions' => [
                    'create' => 'order_create',
                    'update' => 'order_update',
                    'view' => 'order_view',
                    'delete' => 'order_delete',
                    'bulk-delete' => 'order_delete',
                    'index' => ['order_view', 'order_view_all'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new \app\models\OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $page = isset($_GET['p']) ? $_GET['p'] : 0;

        $dataProvider->pagination->page = $page;

        foreach($_GET as $key => $value){
            if($key == 'token' || $key == 'p'){
                continue;
            }

            $dataProvider->query->andFilterWhere(['like', $key, $value]);
        }

        $models = $dataProvider->models;
        $result["action_left"] = [
            [
                "url" => "/update",
                "icon" => "fa fa-pencil",
                "color" => "#4B78BC"
            ],
            [
                "url" => "/delete",
                "icon" => "fa fa-trash",
                "color" => "#BC4B4B"
            ],

        ];
        $result["action_right"] = [
            [
                "url" => "/view",
                "icon" => "fa fa-eye",
                "color" => "#54BC4B"
            ],
        ];
        
        $result["form"] = [
   
            [
                "name" => 'type_repair_id',
                'label' => Yii::t('app', 'Тип'),
                "type" => "books",
                "filter" => "name",
                "url" => "type-repair/index"
            ],               [
                "name" => 'asc',
                'label' => Yii::t('app', 'Заказ наряд'),
                "type" => "input"
            ],   
            [
                "name" => 'nomenklatura_id',
                'label' => Yii::t('app', 'Номенклатура'),
                "type" => "books",
                "filter" => "name",
                "url" => "nomenclature/index"
            ],               [
                "name" => 'sn',
                'label' => Yii::t('app', 'Серийный номер'),
                "type" => "input"
            ],   
            [
                "name" => 'status_id',
                'label' => Yii::t('app', 'Статус'),
                "type" => "books",
                "filter" => "name",
                "url" => "status/index"
            ],               [
                "name" => 'komplektaciya',
                'label' => Yii::t('app', 'Комплектация'),
                "type" => "input"
            ],            [
                "name" => 'vneshniy_vid',
                'label' => Yii::t('app', 'Внешний вид'),
                "type" => "input"
            ],   
            [
                "name" => 'data_sale',
                'label' => Yii::t('app', 'Дата продажи'),
                "type" => "date"
            ],     
            [
                "name" => 'create_at',
                'label' => Yii::t('app', 'Дата приема'),
                "type" => "date"
            ],     
            [
                "name" => 'client_id',
                'label' => Yii::t('app', 'Клиент'),
                "type" => "books",
                "filter" => "name",
                "url" => "client/index"
            ],               [
                "name" => 'repair_status',
                'label' => Yii::t('app', 'Состояние ремонта'),
                "type" => "input"
            ],            [
                "name" => 'type_work',
                'label' => Yii::t('app', 'Тип работы'),
                "type" => "input"
            ],            [
                "name" => 'kontakty',
                'label' => Yii::t('app', 'Контакты'),
                "type" => "input"
            ],            [
                "name" => 'fault',
                'label' => Yii::t('app', 'Неисправность'),
                "type" => "input"
            ],   
            [
                "name" => 'photo',
                'label' => Yii::t('app', 'Фото'),
                "type" => "file"
            ],                      [
                "name" => 'number_sale',
                'label' => Yii::t('app', 'Заказ-наряд продавца'),
                "type" => "input"
            ],            [
                "name" => 'comment',
                'label' => Yii::t('app', 'Комментарий'),
                "type" => "input"
            ],   
            [
                "name" => 'service_id',
                'label' => Yii::t('app', 'СЦ'),
                "type" => "books",
                "filter" => "name",
                "url" => "service/index"
            ],               [
                "name" => 'comment_job',
                'label' => Yii::t('app', 'Комментарий к выполненной работе'),
                "type" => "input"
            ],            [
                "name" => 'nomer_otpravleniya',
                'label' => Yii::t('app', 'Номер отправления'),
                "type" => "input"
            ],            [
                "name" => 'zavodskoy_nomer',
                'label' => Yii::t('app', 'заводской номер'),
                "type" => "input"
            ],   
            [
                "name" => 'zip',
                'label' => Yii::t('app', 'ЗИП'),
                "type" => "books",
                "filter" => "name",
                "url" => "spare/index"
            ],      
            [
                "name" => 'prichina_vydachi_akta',
                'label' => Yii::t('app', 'Причина выдачи акта'),
                "type" => "text"
            ],               
        ];
    
        array_walk($models, function(&$model){
            if ($model->type_repair_id) $model->type_repair_id = $model->typeRepair->name;
            if ($model->nomenklatura_id) $model->nomenklatura_id = $model->nomenklatura->name;
            if ($model->status_id) $model->status_id = $model->status->name;
            if ($model->client_id) $model->client_id = $model->client->name;
            if ($model->service_id) $model->service_id = $model->service->name;
            if ($model->zip) $model->zip = $model->zip->name;
            
        });
        $result["data"] = $models;
        return $result;
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $this->rawData];
        $model = new Order();

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return ["error" => $model->errors];
        }

        return ['result' => false];
    }

    /**
    * @return mixed
    */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $data = ['model' => $this->rawData];
        $model = Order::findOne($id);

        if($model->load($data, 'model') && $model->validate())
        {
            $model->save(false);
            return $model;
        } else {
            return ["error" => $model->errors];
        }

        return ['result' => false];
    }

    public function actionView($id)
    {
        $model = Order::findOne($id);

        $result["form"] = [            [
                "name" => 'type_repair_id',
                'label' => Yii::t('app', 'Тип'),
                "type" => "input"
            ],            [
                "name" => 'asc',
                'label' => Yii::t('app', 'Заказ наряд'),
                "type" => "input"
            ],            [
                "name" => 'nomenklatura_id',
                'label' => Yii::t('app', 'Номенклатура'),
                "type" => "input"
            ],            [
                "name" => 'sn',
                'label' => Yii::t('app', 'Серийный номер'),
                "type" => "input"
            ],            [
                "name" => 'status_id',
                'label' => Yii::t('app', 'Статус'),
                "type" => "input"
            ],            [
                "name" => 'komplektaciya',
                'label' => Yii::t('app', 'Комплектация'),
                "type" => "input"
            ],            [
                "name" => 'vneshniy_vid',
                'label' => Yii::t('app', 'Внешний вид'),
                "type" => "input"
            ],   
            [
                "name" => 'data_sale',
                'label' => Yii::t('app', 'Дата продажи'),
                "type" => "date"
            ],      
            [
                "name" => 'create_at',
                'label' => Yii::t('app', 'Дата приема'),
                "type" => "date"
            ],               [
                "name" => 'client_id',
                'label' => Yii::t('app', 'Клиент'),
                "type" => "input"
            ],            [
                "name" => 'repair_status',
                'label' => Yii::t('app', 'Состояние ремонта'),
                "type" => "input"
            ],            [
                "name" => 'type_work',
                'label' => Yii::t('app', 'Тип работы'),
                "type" => "input"
            ],            [
                "name" => 'kontakty',
                'label' => Yii::t('app', 'Контакты'),
                "type" => "input"
            ],            [
                "name" => 'fault',
                'label' => Yii::t('app', 'Неисправность'),
                "type" => "input"
            ],            [
                "name" => 'photo',
                'label' => Yii::t('app', 'Фото'),
                "type" => "input"
            ],            [
                "name" => 'number_sale',
                'label' => Yii::t('app', 'Заказ-наряд продавца'),
                "type" => "input"
            ],            [
                "name" => 'comment',
                'label' => Yii::t('app', 'Комментарий'),
                "type" => "input"
            ],            [
                "name" => 'service_id',
                'label' => Yii::t('app', 'СЦ'),
                "type" => "input"
            ],            [
                "name" => 'comment_job',
                'label' => Yii::t('app', 'Комментарий к выполненной работе'),
                "type" => "input"
            ],            [
                "name" => 'nomer_otpravleniya',
                'label' => Yii::t('app', 'Номер отправления'),
                "type" => "input"
            ],            [
                "name" => 'zavodskoy_nomer',
                'label' => Yii::t('app', 'заводской номер'),
                "type" => "input"
            ],            [
                "name" => 'zip',
                'label' => Yii::t('app', 'ЗИП'),
                "type" => "input"
            ],   
            [
                "name" => 'prichina_vydachi_akta',
                'label' => Yii::t('app', 'Причина выдачи акта'),
                "type" => "text"
            ],               
        ];
        
        $result["mini_menu"] = [
            'items' => [
                ['label' => Yii::t('app', 'Работы'),  'url' => ['/job?order_id='.$model->id]],
                   
            
        ]];
        $result["data"] = $model;
        return $result;
    }

    public function actionDelete($id)
    {
        $model = Order::findOne($id);

        if($model){
            $model->delete();
        }

        return ['success' => true];
    }

    public function actionFile()
    {
        $file = \yii\web\UploadedFile::getInstanceByName("file");

        if($file){
            if(is_dir('uploads') == false){
                mkdir('uploads');
            }

            $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$file->extension;

            $file->saveAs($path);

            return ['link' => $path];
        }

        return ['link' => null];
    }

    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;

        $content = file_get_contents('php://input');
        $this->rawData = json_decode($content, true);

        if($action->id != 'login')
        {
            $token = null;
            if(isset($_GET['token'])){
                $token = $_GET['token'];
            } elseif(isset($this->rawData['token'])){
                $token = $this->rawData['token'];
            }

            if($token == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Токен не указан');
            }

            $token = explode('||', $token);

            $user = \app\models\User::find()->where(['login' => $token[0], 'password_hash' => $token[1]])->one();

            if($user == null){
                Yii::$app->response->format = Response::FORMAT_JSON;
                throw new \yii\web\BadRequestHttpException('Неверный логин или пароль');
            }

            Yii::$app->user->login($user, 0);
        }

        return parent::beforeAction($action);
    }
}