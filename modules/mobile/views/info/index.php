	
Просмотр Статуса<br>
api/status/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Status, Status, ...]<br>
<br>
Создание Статуса<br>
api/status/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-info_admin (Чекбокс) Уведомление админу<br>
-info_master (Чекбокс) Уведомление мастеру<br>
-info_client (Чекбокс) Уведомление клиенту<br>
-text_admin (Большое текстовое поле) Текст уведомления админу<br>
-text_master (Большое текстовое поле) Текст уведомления мастеру<br>
-text_client (Большое текстовое поле) Текст уведомления клиенту<br>
-is_archive (Чекбокс) Отправить в архив<br>
-is_write (Чекбокс) Списать со склада<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Статус<br>
api/status/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-info_admin (Чекбокс) Уведомление админу<br>
-info_master (Чекбокс) Уведомление мастеру<br>
-info_client (Чекбокс) Уведомление клиенту<br>
-text_admin (Большое текстовое поле) Текст уведомления админу<br>
-text_master (Большое текстовое поле) Текст уведомления мастеру<br>
-text_client (Большое текстовое поле) Текст уведомления клиенту<br>
-is_archive (Чекбокс) Отправить в архив<br>
-is_write (Чекбокс) Списать со склада<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Статуса<br>
api/status/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Статус<br>
api/status/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр СЦ<br>
api/service/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Service, Service, ...]<br>
<br>
Создание СЦ<br>
api/service/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-city (Текст) Город<br>
-adress (Текст)  Адрес<br>
-responsible (Текст) Ответственный<br>
-phone (Текст) Телефон<br>
-post (Текст) Должность<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить СЦ<br>
api/service/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-city (Текст) Город<br>
-adress (Текст)  Адрес<br>
-responsible (Текст) Ответственный<br>
-phone (Текст) Телефон<br>
-post (Текст) Должность<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр СЦ<br>
api/service/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить СЦ<br>
api/service/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Контрагента<br>
api/client/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Client, Client, ...]<br>
<br>
Создание Контрагента<br>
api/client/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-type* (Выпадающий список) Тип<br>
-yr_adress (Текст) Юридический адрес<br>
-kr_adress (Текст) Адрес корреспонденции<br>
-pos_adress (Текст) Адрес посещения<br>
-all_phone (Текст) Общий контактный телефон<br>
-email (Текст) Электронная почта для клиентов<br>
-manag (Текст) Ответственный менеджер<br>
-manag_phone (Текст) Номер<br>
-manag_email (Текст) Электронная почта<br>
-inn (Текст) ИНН<br>
-bik (Текст) БИК<br>
-schet (Текст) Счет<br>
-kor_schet (Текст) Кор.счет<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Контрагента<br>
api/client/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-type* (Выпадающий список) Тип<br>
-yr_adress (Текст) Юридический адрес<br>
-kr_adress (Текст) Адрес корреспонденции<br>
-pos_adress (Текст) Адрес посещения<br>
-all_phone (Текст) Общий контактный телефон<br>
-email (Текст) Электронная почта для клиентов<br>
-manag (Текст) Ответственный менеджер<br>
-manag_phone (Текст) Номер<br>
-manag_email (Текст) Электронная почта<br>
-inn (Текст) ИНН<br>
-bik (Текст) БИК<br>
-schet (Текст) Счет<br>
-kor_schet (Текст) Кор.счет<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Контрагента<br>
api/client/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Контрагента<br>
api/client/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Номенклатуры<br>
api/nomenclature/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Nomenclature, Nomenclature, ...]<br>
<br>
Создание Номенклатуры<br>
api/nomenclature/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-code* (Текст) Артикул<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Номенклатуру<br>
api/nomenclature/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-code* (Текст) Артикул<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Номенклатуры<br>
api/nomenclature/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Номенклатуру<br>
api/nomenclature/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Тип ремонта<br>
api/type-repair/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[TypeRepair, TypeRepair, ...]<br>
<br>
Создание Тип ремонта<br>
api/type-repair/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-sum (Номер) Стоимость<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Тип ремонта<br>
api/type-repair/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-sum (Номер) Стоимость<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Тип ремонта<br>
api/type-repair/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Тип ремонта<br>
api/type-repair/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Запчасти<br>
api/spare/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Spare, Spare, ...]<br>
<br>
Создание Запчасти<br>
api/spare/create<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-nomenklatura (Связь со справочником) Номенклатура<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Запчасти<br>
api/spare/update<br>
[<span style="color: red;">POST</span>]<br>
-name* (Текст) Наименование<br>
-nomenklatura (Связь со справочником) Номенклатура<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Запчасти<br>
api/spare/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Запчасти<br>
api/spare/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Заявок<br>
api/order/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Order, Order, ...]<br>
<br>
Создание Заявок<br>
api/order/create<br>
[<span style="color: red;">POST</span>]<br>
-type_repair_id (Связь со справочником) Тип<br>
-asc (Текст) Заказ наряд<br>
-nomenklatura_id (Связь со справочником) Номенклатура<br>
-sn (Текст) Серийный номер<br>
-komplektaciya (Текст) Комплектация<br>
-vneshniy_vid (Текст) Внешний вид<br>
-data_sale (Дата) Дата продажи<br>
-create_at (Дата) Дата приема<br>
-client_id (Связь со справочником) Клиент<br>
-repair_status (Текст) Состояние ремонта<br>
-type_work (Связь со справочником (теги)) Тип работы<br>
-sum (Номер) Стоимость ремонта<br>
-comment_job (Текст) Комментарий к выполненной работе<br>
-injener_id (Связь со справочником) Инженер<br>
-kontakty (Текст) Контакты<br>
-fault (Текст) Неисправность<br>
-service_id (Связь со справочником) СЦ<br>
-photo (Файл) Фото<br>
-user_id (Связь со справочником) Пользователь<br>
-number_sale (Текст) Заказ-наряд продавца<br>
-adress_del (Текст) Адрес доставки<br>
-status_id (Связь со справочником) Статус<br>
-comment (Текст) Комментарий<br>
-nomer_otpravleniya (Текст) Номер отправления<br>
-zip (Связь со справочником) ЗИП<br>
-zavodskoy_nomer (Текст) заводской номер<br>
-prichina_vydachi_akta (Большое текстовое поле) Причина выдачи акта<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Заявки<br>
api/order/update<br>
[<span style="color: red;">POST</span>]<br>
-type_repair_id (Связь со справочником) Тип<br>
-asc (Текст) Заказ наряд<br>
-nomenklatura_id (Связь со справочником) Номенклатура<br>
-sn (Текст) Серийный номер<br>
-komplektaciya (Текст) Комплектация<br>
-vneshniy_vid (Текст) Внешний вид<br>
-data_sale (Дата) Дата продажи<br>
-create_at (Дата) Дата приема<br>
-client_id (Связь со справочником) Клиент<br>
-repair_status (Текст) Состояние ремонта<br>
-type_work (Связь со справочником (теги)) Тип работы<br>
-sum (Номер) Стоимость ремонта<br>
-comment_job (Текст) Комментарий к выполненной работе<br>
-injener_id (Связь со справочником) Инженер<br>
-kontakty (Текст) Контакты<br>
-fault (Текст) Неисправность<br>
-service_id (Связь со справочником) СЦ<br>
-photo (Файл) Фото<br>
-user_id (Связь со справочником) Пользователь<br>
-number_sale (Текст) Заказ-наряд продавца<br>
-adress_del (Текст) Адрес доставки<br>
-status_id (Связь со справочником) Статус<br>
-comment (Текст) Комментарий<br>
-nomer_otpravleniya (Текст) Номер отправления<br>
-zip (Связь со справочником) ЗИП<br>
-zavodskoy_nomer (Текст) заводской номер<br>
-prichina_vydachi_akta (Большое текстовое поле) Причина выдачи акта<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Заявок<br>
api/order/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Заявки<br>
api/order/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Работ<br>
api/job/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Job, Job, ...]<br>
<br>
Создание Работ<br>
api/job/create<br>
[<span style="color: red;">POST</span>]<br>
-typ_id (Связь со справочником) Тип<br>
-replace_id (Текст) Заменено<br>
-spare_id (Связь со справочником) Запчасть остается<br>
-spare_old_id (Связь со справочником) Запчасть утиль<br>
-order_id (Связь со справочником) Заявка<br>
-sum (Номер) Стоимость<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Работы<br>
api/job/update<br>
[<span style="color: red;">POST</span>]<br>
-typ_id (Связь со справочником) Тип<br>
-replace_id (Текст) Заменено<br>
-spare_id (Связь со справочником) Запчасть остается<br>
-spare_old_id (Связь со справочником) Запчасть утиль<br>
-order_id (Связь со справочником) Заявка<br>
-sum (Номер) Стоимость<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Работ<br>
api/job/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Работы<br>
api/job/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
	
Просмотр Поставки<br>
api/deliver/index<br>
[<span style="color: blue;">GET</span>]<br>
ОТДАЕТ:<br>
[Deliver, Deliver, ...]<br>
<br>
Создание Поставки<br>
api/deliver/create<br>
[<span style="color: red;">POST</span>]<br>
-nomenklatura_id* (Связь со справочником) Номенклатура<br>
-sn (Текст) Серийник<br>
-meriynik (Текст) Мерийник<br>
-origin (Чекбокс) Оригинал<br>
-garantiya (Номер) Гарантия<br>
-partiya (Текст) Партия<br>
-count (Номер) Кол-во<br>
-create_at (Дата и время) Создан<br>
-who_id (Связь со справочником) Кто создал<br>
-sc_id (Связь со справочником) СЦ<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Изменить Поставки<br>
api/deliver/update<br>
[<span style="color: red;">POST</span>]<br>
-nomenklatura_id* (Связь со справочником) Номенклатура<br>
-sn (Текст) Серийник<br>
-meriynik (Текст) Мерийник<br>
-origin (Чекбокс) Оригинал<br>
-garantiya (Номер) Гарантия<br>
-partiya (Текст) Партия<br>
-count (Номер) Кол-во<br>
-create_at (Дата и время) Создан<br>
-who_id (Связь со справочником) Кто создал<br>
-sc_id (Связь со справочником) СЦ<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Просмотр Поставки<br>
api/deliver/view<br>
[<span style="color: blue;">GET</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
Удалить Поставки<br>
api/deliver/delete<br>
[<span style="color: red;">POST</span>]<br>
- id (Число) ID<br>
ОТДАЕТ:<br>
{success: true}<br>
<br>
