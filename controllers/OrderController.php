<?php

namespace app\controllers;

use Yii;
use app\behaviors\RoleBehavior;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use app\models\Template;
use app\components\TagsHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell;
use app\models\Client;
use app\models\Nomenclature;
use yii\helpers\ArrayHelper;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => ['application-form'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'role' => [
                'class' => RoleBehavior::class,
                'instanceQuery' => \app\models\Order::find(),
                'actions' => [
                    'create' => 'order_create',
                    'update' => 'order_update',
                    'view' => 'order_view',
                    'delete' => 'order_delete',
                    'bulk-delete' => 'order_delete',
                    'index' => ['order_view', 'order_view_all'],
                ],
            ],
        ];
    }
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }
            /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionNotFin()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['!=','status_id',[6,7,8]]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }    

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionApInJob()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',1]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }    

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionInJob()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',2]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }    

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionZip()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',3]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }    

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionZipOut()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',4]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionApprov()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',5]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionCancel()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',6]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionFin()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',7]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }


    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionDel()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['=','status_id',8]);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionAll()
    {    
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allDataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allDataProvider' => $allDataProvider,
        ]);
    }
        public function actionAdd()
    {
        $request=Yii::$app->request;
        $model = new Order();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->filePhoto = UploadedFile::getInstance($model, 'filePhoto');
            $error = 0;
            $success = 0;
            if (!empty($model->filePhoto)) {
                $filename = 'uploads/'.$model->filePhoto;
                $model->filePhoto->saveAs($filename);
                $file = fopen($filename, 'r');
                if($file) {
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                $iq = 0;
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    $nrColumns = ord($highestColumn) - 64;
                    for ($row = 2; $row <= $highestRow; ++ $row) {

                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        if (!$cell->getValue()) {
                            continue;
                        }
                        $newModel = new Order();

                        $cell = $worksheet->getCellByColumnAndRow(0, $row);
                        $client = Client::find()->where(['name' => (string)trim($cell->getValue())])->one();
                        if (!$client) {
                            $client = new Client([
                                'name' => (string)trim($cell->getValue()),
                                'type' => 'ООО',
                            ]);
                            $client->save(false);
                        }
                        $newModel->client_id  = $client->id;

                        $cell = $worksheet->getCellByColumnAndRow(1, $row);
                        $newModel->asc  = (string)trim($cell->getValue());


                        $cell = $worksheet->getCellByColumnAndRow(2, $row);
                        $nom = Nomenclature::find()->where(['code' => (string)trim($cell->getValue())])->one();
                        if (!$nom) {
                            $nom = new Nomenclature([
                                'name' => (string)trim($cell->getValue()),
                                'code' => (string)trim($cell->getValue()),
                            ]);
                            $nom->save(false);
                        }
                        $newModel->nomenklatura_id  = $nom->id;


                        $cell = $worksheet->getCellByColumnAndRow(3, $row);
                        $newModel->sn  = (string)trim($cell->getValue());



                        $cell = $worksheet->getCellByColumnAndRow(4, $row);
                        $unix_date = ((int)$cell->getValue() - 25569) * 86400;
                        // $excel_date = 25569 + ($unix_date / 86400);
                        // $unix_date = ($$cell->getValue() - 25569) * 86400;
                        // \Yii::warning(\PHPExcel_Shared_Date::ExcelToPHP($cell->getFormattedValue()));
                        $newModel->data_sale  = date("Y-m-d", $unix_date);


                        $cell = $worksheet->getCellByColumnAndRow(5, $row);

                        $unix_date = ((int)$cell->getValue() - 25569) * 86400;
                        // \Yii::warning(\PHPExcel_Shared_Date::ExcelToPHP($cell->getFormattedValue()));
                        $newModel->create_at  = date("Y-m-d", $unix_date);
                        $cell = $worksheet->getCellByColumnAndRow(6, $row);
                        $newModel->repair_status  = (string)trim($cell->getValue());


                        $cell = $worksheet->getCellByColumnAndRow(7, $row);
                        $newModel->fault  = (string)trim($cell->getValue());

                        $cell = $worksheet->getCellByColumnAndRow(8, $row);
                        $newModel->comment_job  = (string)trim($cell->getValue());

                        $cell = $worksheet->getCellByColumnAndRow(9, $row);
                        $newModel->comment  = (string)trim($cell->getValue());



                        if (!$newModel->save()) {
                            \Yii::warning($newModel->errors);
                            $error++;
                        } else {
                            $success++;
                        }
                    }
                    break;
                }

                    return [
                        'forceReload'=>'#crud-datatable-order-pjax',
                        'title'=> "Загружения",
                        'content'=>"Удачно загруженно: {$success} <br/> Ошибка загрузки: {$error}",
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default','data-dismiss'=>"modal"])
                    ];
                    // exit;
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'forceClose'=>true,
                    ];   
                } else {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Загружения",
                        'content'=>"<span class='text-danger'>Ошибка при загрузке файла</span>",
                        'footer'=> Html::button('Закрыть',['class'=>'btn btn-default','data-dismiss'=>"modal"])
                    ];
                }
            } else {
                return [
                    'title'=> "<span class='text-danger'>Выберите файл</span>",
                    'size'=>'normal',
                    'content'=>$this->renderAjax('add', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary','data-dismiss'=>"modal"]).
                                Html::a('Скачать шаблон', '/template.xlsx', ['class'=>'btn btn-warning', 'download' => true]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                    ];
            }
        }
    }


    public function actionUploadFile()
    {
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $fileName = Yii::$app->security->generateRandomString();
        if (is_dir('uploads') == false) {
            mkdir('uploads');
        }
        $uploadPath = 'uploads';
        if (isset($_FILES['file'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = $uploadPath . '/' . $fileName . '.' . $file->extension;
            
            $file->saveAs($path);
            $base = log($file->size, 1024);
            $suffixes = array('Bytes', 'Kb', 'Mb', 'Gb', 'Tb');
            $size = round(pow(1024, $base - floor($base)), 2) .' '. $suffixes[floor($base)];
            return [
                'name' => $file->name,
                'url' => '/'.$path,
                'size' => $size,
            ];
        }
    }

    public function actionImageDelete($name, $id = null)
    {
        $path = substr($name, 1);
        if (is_file($path)) {
            unlink($path);
        }

        if($id){
            $model = $this->findModel($id);
            $file = json_decode($model->photo, true);
            $file = array_filter($file, function($model) use($name){
                return $name != $model['url'];
            });
            $model->photo = json_encode($file);
            $model->save(false);
        }
        return null;
    }



    /**
     * Displays a single Order model.
     * 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);


            $jobSearchModel = new \app\models\JobSearch();
        $jobDataProvider = $jobSearchModel->search(Yii::$app->request->queryParams);
        $jobDataProvider->query->andWhere(['order_id' => $id]);




        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заявки #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
			'jobSearchModel'=>$jobSearchModel,
			'jobDataProvider'=>$jobDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update', 'id' => $model->id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
			'jobSearchModel'=>$jobSearchModel,
			'jobDataProvider'=>$jobDataProvider,
            ]);
        }
    }


    public function actionExcel()
    {
// $models = HistoryIncosai::find()->all();
        // $models = HistoryIncosai::find()->asArray()->all();

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination = false;

        $models = $dataProvider->models;

        $columns = require('../views/order/_columns_excel.php');

        $data = [];

        $labels = [];
        foreach ($columns as $column) {

            if($column['class'] == 'kartik\grid\SerialColumn'){
                $labels[] = "№ п/п";
                continue;
            }


            if(isset($column['label'])){
                $labels[] = $column['label'];
            } else {
                if(isset($column['attribute'])){
                    $labels[] = (new Order())->getAttributeLabel($column['attribute']);
                } else {
                    $labels[] = null;
                }
            }
        }

        $data[] = $labels;



        $counter = 1;
        foreach ($models as $model) {
            $row = [];
            foreach ($columns as $column) {

                if($column['class'] == 'kartik\grid\SerialColumn'){
                    $row[] = $counter;
                    continue;
                }

                if(isset($column['content'])){

                } elseif(isset($column['value'])){

                    if(is_string($column['value'])){
                        $row[] = ArrayHelper::getValue($model, $column['value']);
                    } else {
                        // $row[] = call_user_func($column['value'], );
                    }

                } else {
                    if(isset($column['attribute'])){
                        $row[] = ArrayHelper::getValue($model, $column['attribute']);
                    } else {
                        $row[] = null;                        
                    }
                }
            }

            $data[] = $row;
            $counter++;
        }

        // \yii\helpers\VarDumper::dump($data, 10, true);
        // exit;

// Генерация Excel
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $objGet = $objPHPExcel->getActiveSheet();


        $styleForHeaders = array('font' => array('size' => 10,'bold' => true,'color' => array('rgb' => '000000'), 'name' => 'Times New Roman'), 'alignment' => array(
            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
        ));
        $styleForDefault = array('font' => array('size' => 10,'bold' => false,'color' => array('rgb' => '000000'), 'name' => 'Times New Roman'));
        $styleForSmall = array('font' => array('size' => 9,'bold' => false,'color' => array('rgb' => '000000'), 'name' => 'Times New Roman'));



//        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);

//        $objGet->setCellValueByColumnAndRow(0, 1, 'Привет как дела');

        $objGet->getColumnDimension('A')->setWidth(4);
        $objGet->getColumnDimension('B')->setWidth(8);
        $objGet->getColumnDimension('C')->setWidth(14);
        $objGet->getColumnDimension('D')->setWidth(10);
        $objGet->getColumnDimension('E')->setWidth(20);
        $objGet->getColumnDimension('F')->setWidth(16);
        $objGet->getColumnDimension('G')->setWidth(20);
        $objGet->getColumnDimension('H')->setWidth(20);
        $objGet->getColumnDimension('I')->setWidth(20);
        $objGet->getColumnDimension('J')->setWidth(20);
        $objGet->getColumnDimension('L')->setWidth(20);
        $objGet->getColumnDimension('M')->setWidth(20);
        $objGet->getColumnDimension('N')->setWidth(20);
        $objGet->getColumnDimension('O')->setWidth(20);
        $objGet->getColumnDimension('P')->setWidth(20);
        $objGet->getColumnDimension('Q')->setWidth(20);
        $objGet->getColumnDimension('R')->setWidth(20);
        $objGet->getColumnDimension('S')->setWidth(20);
        $objGet->getColumnDimension('T')->setWidth(20);
        $objGet->getColumnDimension('U')->setWidth(20);
        $objGet->getColumnDimension('V')->setWidth(20);
        $objGet->getColumnDimension('W')->setWidth(20);
        $objGet->getColumnDimension('X')->setWidth(20);
        $objGet->getColumnDimension('Y')->setWidth(20);
        $objGet->getColumnDimension('Z')->setWidth(20);
        $objGet->getColumnDimension('AA')->setWidth(20);
        $objGet->getColumnDimension('AB')->setWidth(20);
        $objGet->getColumnDimension('AC')->setWidth(20);

        $objGet->mergeCells('A1:G1');
        $objGet->mergeCells('B3:G3');
        $objGet->mergeCells('B4:G4');

        $objGet->setCellValueByColumnAndRow(0, 1, "Акт приема-передачи от ".date('dmy'));
        $objGet->getStyleByColumnAndRow(0, 1)->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));

        $objGet->setCellValueByColumnAndRow(1, 3, "Отправитель: ООО \"Ромбика\", Москва Пятницкая ул.Д71 кор.5 стр.2 офис 500");
        $objGet->getStyleByColumnAndRow(1, 3)->applyFromArray($styleForSmall);

        $objGet->setCellValueByColumnAndRow(1, 4, "Получатель: ".ArrayHelper::getValue($models[0], 'client.name').', '.ArrayHelper::getValue($models[0], 'client.yr_adress'));
        $objGet->getStyleByColumnAndRow(1, 4)->applyFromArray($styleForSmall);

        for ($i = 0; $i <= count($data); $i++)
        {
            if(isset($data[$i]) == false){
                continue;
            }

            $row = $data[$i];

            for ($j = 0; $j <= count($row); $j++)
            {
                if(isset($row[$j])){
                    $value = $row[$j];

                    $objGet->setCellValueByColumnAndRow($j, ($i + 6), $value);

                    $objGet->getStyleByColumnAndRow($j, ($i + 6))->applyFromArray($styleForDefault);
                }

                if($i == 0){
                    $objGet->getStyleByColumnAndRow($j, ($i + 6))->applyFromArray($styleForHeaders);
                    $objGet->getStyleByColumnAndRow($j, ($i + 6))->getAlignment()->setWrapText(true);
                }

                $objGet->getStyle("A".($i + 6).":G".($i + 6))->applyFromArray(
                    array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '000000')
                            )
                        )
                    )
                );
            }
        }


        $mrgn = 6 + count($data) + 1;

        $objGet->mergeCells("B{$mrgn}:D{$mrgn}");
        $objGet->mergeCells("F{$mrgn}:G{$mrgn}");

        $objGet->mergeCells("B".($mrgn+2).":D".($mrgn+2));
        $objGet->mergeCells("F".($mrgn+2).":G".($mrgn+2));

        $objGet->mergeCells("B".($mrgn+3).":D".($mrgn+3));
        $objGet->mergeCells("F".($mrgn+3).":G".($mrgn+3));

        $objGet->setCellValueByColumnAndRow(1, $mrgn, "Сдал: ".ArrayHelper::getValue($models[0], 'client.name'));
        $objGet->getStyleByColumnAndRow(1, $mrgn)->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));

        $objGet->setCellValueByColumnAndRow(5, $mrgn, "Получил: ООО \"MBM\"");
        $objGet->getStyleByColumnAndRow(5, $mrgn)->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));


        $objGet->setCellValueByColumnAndRow(1, ($mrgn + 2), "______________/_____________");
        $objGet->getStyleByColumnAndRow(1, ($mrgn + 2))->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));

        $objGet->setCellValueByColumnAndRow(5, ($mrgn + 2), "______________/_____________");
        $objGet->getStyleByColumnAndRow(5, ($mrgn + 2))->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));

        $objGet->setCellValueByColumnAndRow(1, ($mrgn + 3), "\"____\"__________________".date('Y')."г.");
        $objGet->getStyleByColumnAndRow(1, ($mrgn + 3))->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));

        $objGet->setCellValueByColumnAndRow(5, ($mrgn + 3), "\"____\"__________________".date('Y')."г.");
        $objGet->getStyleByColumnAndRow(5, ($mrgn + 3))->applyFromArray(ArrayHelper::merge($styleForSmall, [
        'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            ]));


        $filename = 'file.xlsx';
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter->save('file.xlsx');

        Yii::$app->response->sendFile('file.xlsx');
    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Order();

        if($request->isGet){
            $model->load(Yii::$app->request->queryParams);
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить Заявки",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-order-pjax',
                    'title'=> "Добавить Заявки",
                    'content'=>'<span class="text-success">Создание Заявки успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить Заявки",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Заявки #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-order-pjax',
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Заявки #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    

    public function actionPrint($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $templates = Template::find()->where(['type' => Template::TYPE_ORDER])->all();

        return [
            'title' => 'Печать',
            'content' => $this->renderAjax('@app/views/_print/print-templates', [
                'templates' => $templates,
                'id' => $id,
            ]),
            'footer' => ''
        ];
    }

    public function actionPrintTemplate($pks = null, $template_id = null)
    {
        $claims = Order::find()->where(['id' => explode(',', $pks)])->all();

        $globalContent = '';

        foreach ($claims as $claim) {
            $template = Template::findOne($template_id);

            $content = $template->text;

            $tagHelper = new TagsHelper([
                'printable' => $claim,
                'content' => $content,
            ]);

            $globalContent .= $tagHelper->handle().'<div class="pagebreak" style="page-break-before: always;"></div>';
        }

        return $this->renderPartial('@app/views/_print/print', [
            'content' => $globalContent,
        ]);
    }


    /**
     * Delete an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-order-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkPrint()
    {        
        $request = Yii::$app->request;
        // $pks = explode(',', $request->get( 'pks' )); // Array or selected records primary keys
        $pks = $request->rawBody; // Array or selected records primary keys

        Yii::$app->response->format = Response::FORMAT_JSON;

        $templates = Template::find()->where(['type' => Template::TYPE_ORDER])->all();


        return [
            'title' => 'Печать',
            'content' => $this->renderAjax('@app/views/_print/print-templates', [
                'templates' => $templates,
                'pks' => $pks,
            ]),
            'footer' => ''
        ];
    }

     /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-order-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
