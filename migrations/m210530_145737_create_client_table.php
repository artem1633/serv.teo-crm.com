<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_145737_create_client_table`.
 */
class m210530_145737_create_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'type' => $this->string()->comment('Тип'),
            'yr_adress' => $this->string()->comment('Юридический адрес'),
            'kr_adress' => $this->string()->comment('Адрес корреспонденции'),
            'pos_adress' => $this->string()->comment('Адрес посещения'),
            'all_phone' => $this->string()->comment('Общий контактный телефон'),
            'email' => $this->string()->comment('Электронная почта для клиентов'),
            'manag' => $this->string()->comment('Ответственный менеджер'),
            'manag_phone' => $this->string()->comment('Номер'),
            'manag_email' => $this->string()->comment('Электронная почта'),
            'inn' => $this->string()->comment('ИНН'),
            'bik' => $this->string()->comment('БИК'),
            'schet' => $this->string()->comment('Счет'),
            'kor_schet' => $this->string()->comment('Кор.счет'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('client');
    }
}
