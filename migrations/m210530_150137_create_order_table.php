<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_150137_create_order_table`.
 */
class m210530_150137_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'type_repair_id' => $this->integer()->comment('Тип'),
            'asc' => $this->string()->comment('Заказ наряд'),
            'nomenklatura_id' => $this->integer()->comment('Номенклатура'),
            'sn' => $this->string()->comment('Серийный номер'),
            'komplektaciya' => $this->string()->comment('Комплектация'),
            'vneshniy_vid' => $this->string()->comment('Внешний вид'),
            'data_sale' => $this->date()->comment('Дата продажи'),
            'create_at' => $this->date()->comment('Дата приема'),
            'client_id' => $this->integer()->comment('Клиент'),
            'repair_status' => $this->string()->comment('Состояние ремонта'),
            'type_work' => $this->text()->comment('Тип работы'),
            'sum' => $this->double()->comment('Стоимость ремонта'),
            'comment_job' => $this->string()->comment('Комментарий к выполненной работе'),
            'injener_id' => $this->integer()->comment('Инженер'),
            'kontakty' => $this->string()->comment('Контакты'),
            'fault' => $this->string()->comment('Неисправность'),
            'service_id' => $this->integer()->comment('СЦ'),
            'photo' => $this->string()->comment('Фото'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'number_sale' => $this->string()->comment('Заказ-наряд продавца'),
            'adress_del' => $this->string()->comment('Адрес доставки'),
            'status_id' => $this->integer()->comment('Статус'),
            'comment' => $this->string()->comment('Комментарий'),
            'nomer_otpravleniya' => $this->string()->comment('Номер отправления'),
            'zip' => $this->integer()->comment('ЗИП'),
            'zavodskoy_nomer' => $this->string()->comment('заводской номер'),
            'prichina_vydachi_akta' => $this->binary()->comment('Причина выдачи акта'),
        ]);

        $this->createIndex(
            'idx-order-type_repair_id',
            'order',
            'type_repair_id'
        );
                        
        $this->addForeignKey(
            'fk-order-type_repair_id',
            'order',
            'type_repair_id',
            'type_repair',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-nomenklatura_id',
            'order',
            'nomenklatura_id'
        );
                        
        $this->addForeignKey(
            'fk-order-nomenklatura_id',
            'order',
            'nomenklatura_id',
            'nomenclature',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-client_id',
            'order',
            'client_id'
        );
                        
        $this->addForeignKey(
            'fk-order-client_id',
            'order',
            'client_id',
            'client',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-injener_id',
            'order',
            'injener_id'
        );
                        
        $this->addForeignKey(
            'fk-order-injener_id',
            'order',
            'injener_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-service_id',
            'order',
            'service_id'
        );
                        
        $this->addForeignKey(
            'fk-order-service_id',
            'order',
            'service_id',
            'service',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-user_id',
            'order',
            'user_id'
        );
                        
        $this->addForeignKey(
            'fk-order-user_id',
            'order',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-status_id',
            'order',
            'status_id'
        );
                        
        $this->addForeignKey(
            'fk-order-status_id',
            'order',
            'status_id',
            'status',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-order-zip',
            'order',
            'zip'
        );
                        
        $this->addForeignKey(
            'fk-order-zip',
            'order',
            'zip',
            'spare',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-order-type_repair_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-type_repair_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-nomenklatura_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-nomenklatura_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-client_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-client_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-injener_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-injener_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-service_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-service_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-user_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-user_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-status_id',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-status_id',
            'order'
        );
                        
                        $this->dropForeignKey(
            'fk-order-zip',
            'order'
        );
                        
        $this->dropIndex(
            'idx-order-zip',
            'order'
        );
                        
                        
        $this->dropTable('order');
    }
}
