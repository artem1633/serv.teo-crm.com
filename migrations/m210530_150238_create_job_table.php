<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_150238_create_job_table`.
 */
class m210530_150238_create_job_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('job', [
            'id' => $this->primaryKey(),
            'typ_id' => $this->integer()->comment('Тип'),
            'replace_id' => $this->string()->comment('Заменено'),
            'spare_id' => $this->integer()->comment('Запчасть остается'),
            'spare_old_id' => $this->integer()->comment('Запчасть утиль'),
            'order_id' => $this->integer()->comment('Заявка'),
            'sum' => $this->double()->comment('Стоимость'),
        ]);

        $this->createIndex(
            'idx-job-typ_id',
            'job',
            'typ_id'
        );
                        
        $this->addForeignKey(
            'fk-job-typ_id',
            'job',
            'typ_id',
            'type_repair',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-job-spare_id',
            'job',
            'spare_id'
        );
                        
        $this->addForeignKey(
            'fk-job-spare_id',
            'job',
            'spare_id',
            'spare',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-job-spare_old_id',
            'job',
            'spare_old_id'
        );
                        
        $this->addForeignKey(
            'fk-job-spare_old_id',
            'job',
            'spare_old_id',
            'spare',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-job-order_id',
            'job',
            'order_id'
        );
                        
        $this->addForeignKey(
            'fk-job-order_id',
            'job',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-job-typ_id',
            'job'
        );
                        
        $this->dropIndex(
            'idx-job-typ_id',
            'job'
        );
                        
                        $this->dropForeignKey(
            'fk-job-spare_id',
            'job'
        );
                        
        $this->dropIndex(
            'idx-job-spare_id',
            'job'
        );
                        
                        $this->dropForeignKey(
            'fk-job-spare_old_id',
            'job'
        );
                        
        $this->dropIndex(
            'idx-job-spare_old_id',
            'job'
        );
                        
                        $this->dropForeignKey(
            'fk-job-order_id',
            'job'
        );
                        
        $this->dropIndex(
            'idx-job-order_id',
            'job'
        );
                        
                        
        $this->dropTable('job');
    }
}
