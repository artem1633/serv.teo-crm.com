<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_service}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%service}}`
 */
class m210721_113650_create_junction_table_for_user_and_service_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_service}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'service_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_service-user_id}}',
            '{{%user_service}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_service-user_id}}',
            '{{%user_service}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `service_id`
        $this->createIndex(
            '{{%idx-user_service-service_id}}',
            '{{%user_service}}',
            'service_id'
        );

        // add foreign key for table `{{%service}}`
        $this->addForeignKey(
            '{{%fk-user_service-service_id}}',
            '{{%user_service}}',
            'service_id',
            '{{%service}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_service-user_id}}',
            '{{%user_service}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_service-user_id}}',
            '{{%user_service}}'
        );

        // drops foreign key for table `{{%service}}`
        $this->dropForeignKey(
            '{{%fk-user_service-service_id}}',
            '{{%user_service}}'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            '{{%idx-user_service-service_id}}',
            '{{%user_service}}'
        );

        $this->dropTable('{{%user_service}}');
    }
}
