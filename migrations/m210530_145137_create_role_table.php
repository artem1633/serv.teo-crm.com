<?php
use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m210530_145137_create_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'order_create' => $this->boolean()->defaultValue(false)->comment('Заявки Добавление'),
            'order_update' => $this->boolean()->defaultValue(false)->comment('Заявки Изменение'),
            'order_view' => $this->boolean()->defaultValue(false)->comment('Заявки Просмотр'),
            'order_view_all' => $this->boolean()->defaultValue(false)->comment('Заявки Просмотр всех'),
            'order_delete' => $this->boolean()->defaultValue(false)->comment('Заявки Удалить'),
            'job_create' => $this->boolean()->defaultValue(false)->comment('Работы Добавление'),
            'job_update' => $this->boolean()->defaultValue(false)->comment('Работы Изменение'),
            'job_view' => $this->boolean()->defaultValue(false)->comment('Работы Просмотр'),
            'job_view_all' => $this->boolean()->defaultValue(false)->comment('Работы Просмотр всех'),
            'job_delete' => $this->boolean()->defaultValue(false)->comment('Работы Удалить'),
            'deliver_create' => $this->boolean()->defaultValue(false)->comment('Поставки Добавление'),
            'deliver_update' => $this->boolean()->defaultValue(false)->comment('Поставки Изменение'),
            'deliver_view' => $this->boolean()->defaultValue(false)->comment('Поставки Просмотр'),
            'deliver_view_all' => $this->boolean()->defaultValue(false)->comment('Поставки Просмотр всех'),
            'deliver_delete' => $this->boolean()->defaultValue(false)->comment('Поставки Удалить'),
            'books' => $this->boolean()->defaultValue(false)->comment('Справочники'),
            'settingsGlob' => $this->boolean()->defaultValue(false)->comment('Настройки'),
        ]);

        $this->insert('role', [
            'name' => 'Администратор',
            'order_create' => 1,
            'order_update' => 1,
            'order_view' => 1,
            'order_view_all' => 1,
            'order_delete' => 1,
            'job_create' => 1,
            'job_update' => 1,
            'job_view' => 1,
            'job_view_all' => 1,
            'job_delete' => 1,
            'deliver_create' => 1,
            'deliver_update' => 1,
            'deliver_view' => 1,
            'deliver_view_all' => 1,
            'deliver_delete' => 1,
            'books' => 1,
            'settingsGlob' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('role');
    }
}
