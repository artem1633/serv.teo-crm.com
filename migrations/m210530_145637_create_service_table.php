<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_145637_create_service_table`.
 */
class m210530_145637_create_service_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('service', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'city' => $this->string()->comment('Город'),
            'adress' => $this->string()->comment(' Адрес'),
            'responsible' => $this->string()->comment('Ответственный'),
            'phone' => $this->string()->comment('Телефон'),
            'post' => $this->string()->comment('Должность'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('service');
    }
}
