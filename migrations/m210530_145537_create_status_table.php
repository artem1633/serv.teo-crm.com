<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_145537_create_status_table`.
 */
class m210530_145537_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'info_admin' => $this->boolean()->comment('Уведомление админу'),
            'info_master' => $this->boolean()->comment('Уведомление мастеру'),
            'info_client' => $this->boolean()->comment('Уведомление клиенту'),
            'text_admin' => $this->binary()->comment('Текст уведомления админу'),
            'text_master' => $this->binary()->comment('Текст уведомления мастеру'),
            'text_client' => $this->binary()->comment('Текст уведомления клиенту'),
            'is_archive' => $this->boolean()->comment('Отправить в архив'),
            'is_write' => $this->boolean()->comment('Списать со склада'),
        ]);

        

$this->insert('status', [ 'name' => 'Принят в ремонт']);
$this->insert('status', [ 'name' => 'В ремонт']);
$this->insert('status', [ 'name' => 'Ожидает ЗИП']);
$this->insert('status', [ 'name' => 'Ожидает ЗИП отправлено']);
$this->insert('status', [ 'name' => 'Ожидают утверждения']);
$this->insert('status', [ 'name' => 'Отклонить']);
$this->insert('status', [ 'name' => 'Завершена']);
$this->insert('status', [ 'name' => 'Удалена']);    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('status');
    }
}
