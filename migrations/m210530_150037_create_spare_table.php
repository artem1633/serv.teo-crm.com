<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_150037_create_spare_table`.
 */
class m210530_150037_create_spare_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('spare', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'nomenklatura' => $this->integer()->comment('Номенклатура'),
        ]);

        $this->createIndex(
            'idx-spare-nomenklatura',
            'spare',
            'nomenklatura'
        );
                        
        $this->addForeignKey(
            'fk-spare-nomenklatura',
            'spare',
            'nomenklatura',
            'nomenclature',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-spare-nomenklatura',
            'spare'
        );
                        
        $this->dropIndex(
            'idx-spare-nomenklatura',
            'spare'
        );
                        
                        
        $this->dropTable('spare');
    }
}
