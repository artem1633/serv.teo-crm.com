<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%nomenclature}}`.
 */
class m211024_155026_add_category_column_to_nomenclature_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('nomenclature', 'category', $this->string()->comment('Категория'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('nomenclature', 'category');
    }
}
