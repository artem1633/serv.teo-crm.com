<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_145837_create_nomenclature_table`.
 */
class m210530_145837_create_nomenclature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nomenclature', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'code' => $this->string()->comment('Артикул'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('nomenclature');
    }
}
