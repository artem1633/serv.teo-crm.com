<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_150338_create_deliver_table`.
 */
class m210530_150338_create_deliver_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('deliver', [
            'id' => $this->primaryKey(),
            'nomenklatura_id' => $this->integer()->comment('Номенклатура'),
            'sn' => $this->string()->comment('Серийник'),
            'meriynik' => $this->string()->comment('Мерийник'),
            'origin' => $this->boolean()->comment('Оригинал'),
            'garantiya' => $this->double()->comment('Гарантия'),
            'partiya' => $this->string()->comment('Партия'),
            'count' => $this->double()->comment('Кол-во'),
            'create_at' => $this->datetime()->comment('Создан'),
            'who_id' => $this->integer()->comment('Кто создал'),
            'sc_id' => $this->integer()->comment('СЦ'),
        ]);

        $this->createIndex(
            'idx-deliver-nomenklatura_id',
            'deliver',
            'nomenklatura_id'
        );
                        
        $this->addForeignKey(
            'fk-deliver-nomenklatura_id',
            'deliver',
            'nomenklatura_id',
            'nomenclature',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-deliver-who_id',
            'deliver',
            'who_id'
        );
                        
        $this->addForeignKey(
            'fk-deliver-who_id',
            'deliver',
            'who_id',
            'user',
            'id',
            'SET NULL'
        );
                        $this->createIndex(
            'idx-deliver-sc_id',
            'deliver',
            'sc_id'
        );
                        
        $this->addForeignKey(
            'fk-deliver-sc_id',
            'deliver',
            'sc_id',
            'service',
            'id',
            'SET NULL'
        );
                        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-deliver-nomenklatura_id',
            'deliver'
        );
                        
        $this->dropIndex(
            'idx-deliver-nomenklatura_id',
            'deliver'
        );
                        
                        $this->dropForeignKey(
            'fk-deliver-who_id',
            'deliver'
        );
                        
        $this->dropIndex(
            'idx-deliver-who_id',
            'deliver'
        );
                        
                        $this->dropForeignKey(
            'fk-deliver-sc_id',
            'deliver'
        );
                        
        $this->dropIndex(
            'idx-deliver-sc_id',
            'deliver'
        );
                        
                        
        $this->dropTable('deliver');
    }
}
