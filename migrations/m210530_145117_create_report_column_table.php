<?php
use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m210530_145117_create_report_column_table extends Migration
{
    /**
     * @inheritdoc 
     */
    public function up()
    {
        $this->createTable('report_column', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'order_type_repair_id' => $this->boolean()->defaultValue(false)->comment('Заявки Тип'),
            'order_asc' => $this->boolean()->defaultValue(false)->comment('Заявки Заказ наряд'),
            'order_nomenklatura_id' => $this->boolean()->defaultValue(false)->comment('Заявки Номенклатура'),
            'order_sn' => $this->boolean()->defaultValue(false)->comment('Заявки Серийный номер'),
            'order_status_id' => $this->boolean()->defaultValue(false)->comment('Заявки Статус'),
            'order_komplektaciya' => $this->boolean()->defaultValue(false)->comment('Заявки Комплектация'),
            'order_vneshniy_vid' => $this->boolean()->defaultValue(false)->comment('Заявки Внешний вид'),
            'order_data_sale' => $this->boolean()->defaultValue(false)->comment('Заявки Дата продажи'),
            'order_create_at' => $this->boolean()->defaultValue(false)->comment('Заявки Дата приема'),
            'order_client_id' => $this->boolean()->defaultValue(false)->comment('Заявки Клиент'),
            'order_repair_status' => $this->boolean()->defaultValue(false)->comment('Заявки Состояние ремонта'),
            'order_type_work' => $this->boolean()->defaultValue(false)->comment('Заявки Тип работы'),
            'order_sum' => $this->boolean()->defaultValue(false)->comment('Заявки Стоимость ремонта'),
            'order_injener_id' => $this->boolean()->defaultValue(false)->comment('Заявки Инженер'),
            'order_kontakty' => $this->boolean()->defaultValue(false)->comment('Заявки Контакты'),
            'order_fault' => $this->boolean()->defaultValue(false)->comment('Заявки Неисправность'),
            'order_photo' => $this->boolean()->defaultValue(false)->comment('Заявки Фото'),
            'order_user_id' => $this->boolean()->defaultValue(false)->comment('Заявки Пользователь'),
            'order_number_sale' => $this->boolean()->defaultValue(false)->comment('Заявки Заказ-наряд продавца'),
            'order_adress_del' => $this->boolean()->defaultValue(false)->comment('Заявки Адрес доставки'),
            'order_comment' => $this->boolean()->defaultValue(false)->comment('Заявки Комментарий'),
            'order_service_id' => $this->boolean()->defaultValue(false)->comment('Заявки СЦ'),
            'order_comment_job' => $this->boolean()->defaultValue(false)->comment('Заявки Комментарий к выполненной работе'),
            'order_nomer_otpravleniya' => $this->boolean()->defaultValue(false)->comment('Заявки Номер отправления'),
            'order_zavodskoy_nomer' => $this->boolean()->defaultValue(false)->comment('Заявки заводской номер'),
            'order_zip' => $this->boolean()->defaultValue(false)->comment('Заявки ЗИП'),
            'order_prichina_vydachi_akta' => $this->boolean()->defaultValue(false)->comment('Заявки Причина выдачи акта'),
            'job_typ_id' => $this->boolean()->defaultValue(false)->comment('Работы Тип'),
            'job_sum' => $this->boolean()->defaultValue(false)->comment('Работы Стоимость'),
            'job_replace_id' => $this->boolean()->defaultValue(false)->comment('Работы Заменено'),
            'job_spare_id' => $this->boolean()->defaultValue(false)->comment('Работы Запчасть остается'),
            'job_spare_old_id' => $this->boolean()->defaultValue(false)->comment('Работы Запчасть утиль'),
            'job_order_id' => $this->boolean()->defaultValue(false)->comment('Работы Заявка'),
            'deliver_nomenklatura_id' => $this->boolean()->defaultValue(false)->comment('Поставки Номенклатура'),
            'deliver_origin' => $this->boolean()->defaultValue(false)->comment('Поставки Оригинал'),
            'deliver_count' => $this->boolean()->defaultValue(false)->comment('Поставки Кол-во'),
            'deliver_sn' => $this->boolean()->defaultValue(false)->comment('Поставки Серийник'),
            'deliver_meriynik' => $this->boolean()->defaultValue(false)->comment('Поставки Мерийник'),
            'deliver_garantiya' => $this->boolean()->defaultValue(false)->comment('Поставки Гарантия'),
            'deliver_partiya' => $this->boolean()->defaultValue(false)->comment('Поставки Партия'),
            'deliver_create_at' => $this->boolean()->defaultValue(false)->comment('Поставки Создан'),
            'deliver_who_id' => $this->boolean()->defaultValue(false)->comment('Поставки Кто создал'),
            'deliver_sc_id' => $this->boolean()->defaultValue(false)->comment('Поставки СЦ'),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('report_column');
    }
}
