<?php

use yii\db\Migration;

/**
 * Handles the creation of table `m210530_145937_create_type_repair_table`.
 */
class m210530_145937_create_type_repair_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('type_repair', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'sum' => $this->double()->comment('Стоимость'),
        ]);

        

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        
        $this->dropTable('type_repair');
    }
}
