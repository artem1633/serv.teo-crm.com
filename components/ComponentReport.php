<?php 
namespace app\components;

use app\models\Order; 
use app\models\Job; 
use app\models\Deliver; 
  

use app\models\forms\ReportSearch;
use kartik\grid\GridView;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\JsExpression;

/**
 * Class ComponentReport
 * @package app\components
 */
class ComponentReport extends Component
{
    const DATE_COLUMNS = [
        'order_type_repair_id',
        'order_asc',
        'order_nomenklatura_id',
        'order_sn',
        'order_status_id',
        'order_komplektaciya',
        'order_vneshniy_vid',
        'order_data_sale',
        'order_create_at',
        'order_client_id',
        'order_repair_status',
        'order_type_work',
        'order_sum',
        'order_injener_id',
        'order_kontakty',
        'order_fault',
        'order_photo',
        'order_user_id',
        'order_number_sale',
        'order_adress_del',
        'order_comment',
        'order_service_id',
        'order_comment_job',
        'order_nomer_otpravleniya',
        'order_zavodskoy_nomer',
        'order_zip',
        'order_prichina_vydachi_akta',
        'job_typ_id',
        'job_sum',
        'job_replace_id',
        'job_spare_id',
        'job_spare_old_id',
        'job_order_id',
        'deliver_nomenklatura_id',
        'deliver_origin',
        'deliver_count',
        'deliver_sn',
        'deliver_meriynik',
        'deliver_garantiya',
        'deliver_partiya',
        'deliver_create_at',
        'deliver_who_id',
        'deliver_sc_id',
  
    ];

    /**
     * @var array
     */
    public $columns;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param boolean $excel
     * @return array
     */
    public function getGridColumns($excel = false)
    {
        $btnClear = '<button class="btn btn-xs btn-default btn-block" onclick="$(this).parent().find(\'input\').val(\'\'); $(this).parent().find(\'input\').one().trigger(\'change\')">×</button>';

        for ($i = 0; $i < count($this->columns); $i++){
            $column = $this->columns[$i];
            if($column['attribute'] == 'order_type_repair_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(TypeRepair::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = TypeRepair::findOne($model['order_type_repair_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_nomenklatura_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Nomenclature::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Nomenclature::findOne($model['order_nomenklatura_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_status_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Status::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Status::findOne($model['order_status_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_client_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Client::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Client::findOne($model['order_client_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_injener_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = User::findOne($model['order_injener_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_user_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = User::findOne($model['order_user_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_service_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Service::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Service::findOne($model['order_service_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'order_zip'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Spare::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Spare::findOne($model['order_zip']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

  
            if($column['attribute'] == 'job_typ_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(TypeRepair::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = TypeRepair::findOne($model['job_typ_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'job_spare_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Spare::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Spare::findOne($model['job_spare_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'job_spare_old_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Spare::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Spare::findOne($model['job_spare_old_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'job_order_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Order::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Order::findOne($model['job_order_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

  
            if($column['attribute'] == 'deliver_nomenklatura_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Nomenclature::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Nomenclature::findOne($model['deliver_nomenklatura_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'deliver_who_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = User::findOne($model['deliver_who_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

            if($column['attribute'] == 'deliver_sc_id'){
                $this->columns[$i]['filter'] = ArrayHelper::map(Service::find()->all(), 'id', 'name');
                $this->columns[$i]['value'] = function($model){
                    $it = Service::findOne($model['deliver_sc_id']);
                    if($it != null){
                        return $it->name;
                    }
                };
                $this->columns[$i]['filterType'] = GridView::FILTER_SELECT2;
                $this->columns[$i]['filterWidgetOptions'] = [
                    'options' => ['prompt' => '', 'multiple' => true],
                    'pluginOptions' => ['allowClear' => true],
                ];
            }

  
  
            if(isset($this->columns[$i]['filterType']) == false && isset($this->columns[$i]['filter']) == false){
                $value = ArrayHelper::getValue($_GET, "ReportSearch.".$this->columns[$i]['attribute']);
                $this->columns[$i]['filterOptions'] = ['style' => 'position: relative;'];
                $this->columns[$i]['filter'] = "<input type=\"text\" value=\"{$value}\" class=\"form-control\" name=\"ReportSearch[".$this->columns[$i]['attribute']."]\">".
                    '<span style="float: left; position: absolute; top: 5px; right: 0; font-size: 15px; cursor: pointer;" onclick="$(this).parent().find(\'input\').val(\'\'); $(this).parent().find(\'input\').one().trigger(\'change\');">x</span>';
            }
        }

        return $this->columns;
    }
}