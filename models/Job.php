<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "job".
*
    * @property int $typ_id Тип
    * @property  $sum Стоимость
    * @property string $replace_id Заменено
    * @property int $spare_id Запчасть остается
    * @property int $spare_old_id Запчасть утиль
    * @property int $order_id Заявка
*/
class Job extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'job';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['typ_id', 'spare_id', 'spare_old_id', 'order_id'], 'integer'],
            [['sum'], 'number'],
            [['replace_id'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'typ_id' => Yii::t('app', 'Тип'),
            'sum' => Yii::t('app', 'Стоимость'),
            'replace_id' => Yii::t('app', 'Заменено'),
            'spare_id' => Yii::t('app', 'Запчасть остается'),
            'spare_old_id' => Yii::t('app', 'Запчасть утиль'),
            'order_id' => Yii::t('app', 'Заявка'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTyp()
    {
        return $this->hasOne(TypeRepair::className(), ['id' => 'typ_id']);
    }

    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSpare()
    {
        return $this->hasOne(Spare::className(), ['id' => 'spare_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSpareOld()
    {
        return $this->hasOne(Spare::className(), ['id' => 'spare_old_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }


}