<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "client".
*
    * @property string $name Наименование
    * @property  $type Тип
    * @property string $yr_adress Юридический адрес
    * @property string $kr_adress Адрес корреспонденции
    * @property string $pos_adress Адрес посещения
    * @property string $all_phone Общий контактный телефон
    * @property string $email Электронная почта для клиентов
    * @property string $manag Ответственный менеджер
    * @property string $manag_phone Номер
    * @property string $manag_email Электронная почта
    * @property string $inn ИНН
    * @property string $bik БИК
    * @property string $schet Счет
    * @property string $kor_schet Кор.счет
*/
class Client extends \yii\db\ActiveRecord
{

    public $fileUploading;
    const OOO = 'ООО';
    const IP = 'ИП';
    const FZ = 'Физ лицо';
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'client';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['name', 'type', 'yr_adress', 'kr_adress', 'pos_adress', 'all_phone', 'email', 'manag', 'manag_phone', 'manag_email', 'inn', 'bik', 'schet', 'kor_schet'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'type' => Yii::t('app', 'Тип'),
            'yr_adress' => Yii::t('app', 'Юридический адрес'),
            'kr_adress' => Yii::t('app', 'Адрес корреспонденции'),
            'pos_adress' => Yii::t('app', 'Адрес посещения'),
            'all_phone' => Yii::t('app', 'Общий контактный телефон'),
            'email' => Yii::t('app', 'Электронная почта для клиентов'),
            'manag' => Yii::t('app', 'Ответственный менеджер'),
            'manag_phone' => Yii::t('app', 'Номер'),
            'manag_email' => Yii::t('app', 'Электронная почта'),
            'inn' => Yii::t('app', 'ИНН
'),
            'bik' => Yii::t('app', 'БИК
'),
            'schet' => Yii::t('app', 'Счет'),
            'kor_schet' => Yii::t('app', 'Кор.счет'),
        ];
    }

    public static function typeLabels() {
        return [
            self::OOO => "ООО",
            self::IP => "ИП",
            self::FZ => "Физ лицо",
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->workTypes != null) {
            $up = false;
            $allSubs = OrderWorkType::find()->where(['order_id' => $this->id])->all();
            foreach ($allSubs as $sub) {
                if (array_search($sub, $this->workTypes) !== false) {
                    continue;

                } else {
                    $sub->delete();
                }
            }

            foreach ($this->workTypes as $subgroup) {

                $subgroup2 = OrderWorkType::find()->where(['order_id' => $this->id, 'work_type_id' => $subgroup])->one();
                if (!$subgroup2) {
                    $up = true;
                    (new OrderWorkType([
                        'work_type_id' => $subgroup,
                        'order_id' => $this->id
                    ]))->save(false);
                }
            }
            if($up == true and $this->isNewRecord == false){
                $a = ArrayHelper::getColumn($allSubs, 'work_type_id');
                $lost = ArrayHelper::getColumn(WorkType::find()->where(['id' => $a])->all(), 'name');
                $a2 = ArrayHelper::getColumn(OrderWorkType::find()->where(['order_id' => $this->id])->all(), 'work_type_id');
                $new = ArrayHelper::getColumn(WorkType::find()->where(['id' => $a2])->all(), 'name');
                $this->setToChangeTable('Типы работ: ',implode(', ', $lost),implode(', ', $new),'измнены');
            }
        }
    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['client_id' => 'id']);
    }

}