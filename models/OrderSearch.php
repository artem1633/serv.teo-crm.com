<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `Order`.
 */
class OrderSearch extends Order
{
    const DATE_ATTRIBUTES = ['data_sale', 'create_at'];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_repair_id', 'nomenklatura_id', 'status_id', 'data_sale', 'create_at', 'client_id', 'type_work', 'sum', 'injener_id', 'photo', 'user_id', 'service_id', 'zip'], 'safe'],
            [['asc', 'sn', 'komplektaciya', 'vneshniy_vid', 'repair_status', 'kontakty', 'fault', 'number_sale', 'adress_del', 'comment', 'comment_job', 'nomer_otpravleniya', 'zavodskoy_nomer', 'prichina_vydachi_akta'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'type_repair_id' => $this->type_repair_id,
            'nomenklatura_id' => $this->nomenklatura_id,
            'status_id' => $this->status_id,
            'client_id' => $this->client_id,
            'injener_id' => $this->injener_id,
            'user_id' => $this->user_id,
            'service_id' => $this->service_id,
            'zip' => $this->zip,
        ]);

        $query->andFilterWhere(['like', 'asc', $this->asc])
            ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'komplektaciya', $this->komplektaciya])
            ->andFilterWhere(['like', 'vneshniy_vid', $this->vneshniy_vid])
            ->andFilterWhere(['like', 'repair_status', $this->repair_status])
            ->andFilterWhere(['like', 'sum', $this->sum])
            ->andFilterWhere(['like', 'kontakty', $this->kontakty])
            ->andFilterWhere(['like', 'fault', $this->fault])
            ->andFilterWhere(['like', 'number_sale', $this->number_sale])
            ->andFilterWhere(['like', 'adress_del', $this->adress_del])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'comment_job', $this->comment_job])
            ->andFilterWhere(['like', 'nomer_otpravleniya', $this->nomer_otpravleniya])
            ->andFilterWhere(['like', 'zavodskoy_nomer', $this->zavodskoy_nomer])
            ->andFilterWhere(['like', 'prichina_vydachi_akta', $this->prichina_vydachi_akta]);

        foreach (self::DATE_ATTRIBUTES as $attr) {
            if($this->$attr)
            {
                $dates = explode(' - ', $this->$attr);
                $query->andWhere(['between', $attr, $dates[0], $dates[1]]);
            }
        }

        return $dataProvider;
    }
}
