<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "deliver".
*
    * @property int $nomenklatura_id Номенклатура
    * @property  $origin Оригинал
    * @property  $count Кол-во
    * @property string $sn Серийник
    * @property string $meriynik Мерийник
    * @property  $garantiya Гарантия
    * @property string $partiya Партия
    * @property  $create_at Создан
    * @property int $who_id Кто создал
    * @property int $sc_id СЦ
*/
class Deliver extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'deliver';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['nomenklatura_id'], 'required'],
            [['nomenklatura_id', 'origin', 'who_id', 'sc_id'], 'integer'],
            [['count', 'garantiya'], 'number'],
            [['sn', 'meriynik', 'partiya', 'create_at'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'nomenklatura_id' => Yii::t('app', 'Номенклатура'),
            'origin' => Yii::t('app', 'Оригинал'),
            'count' => Yii::t('app', 'Кол-во'),
            'sn' => Yii::t('app', 'Серийник от'),
            'meriynik' => Yii::t('app', 'Мерийник'),
            'garantiya' => Yii::t('app', 'Гарантия'),
            'partiya' => Yii::t('app', 'Партия'),
            'create_at' => Yii::t('app', 'Создан'),
            'who_id' => Yii::t('app', 'Кто создал'),
            'sc_id' => Yii::t('app', 'СЦ'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getNomenklatura()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nomenklatura_id']);
    }

    
    
    
    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getWho()
    {
        return $this->hasOne(User::className(), ['id' => 'who_id']);
    }

    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSc()
    {
        return $this->hasOne(Service::className(), ['id' => 'sc_id']);
    }


}