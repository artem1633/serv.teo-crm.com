<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form about `Client`.
 */
class ClientSearch extends Client
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'yr_adress', 'kr_adress', 'pos_adress', 'all_phone', 'email', 'manag', 'manag_phone', 'manag_email', 'inn', 'bik', 'schet', 'kor_schet'], 'string'],
            [['type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'yr_adress', $this->yr_adress])
            ->andFilterWhere(['like', 'kr_adress', $this->kr_adress])
            ->andFilterWhere(['like', 'pos_adress', $this->pos_adress])
            ->andFilterWhere(['like', 'all_phone', $this->all_phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'manag', $this->manag])
            ->andFilterWhere(['like', 'manag_phone', $this->manag_phone])
            ->andFilterWhere(['like', 'manag_email', $this->manag_email])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'bik', $this->bik])
            ->andFilterWhere(['like', 'schet', $this->schet])
            ->andFilterWhere(['like', 'kor_schet', $this->kor_schet]);

        return $dataProvider;
    }
}
