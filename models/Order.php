<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "order".
*
    * @property int $type_repair_id Тип
    * @property string $asc Заказ наряд
    * @property int $nomenklatura_id Номенклатура
    * @property string $sn Серийный номер
    * @property int $status_id Статус
    * @property string $komplektaciya Комплектация
    * @property string $vneshniy_vid Внешний вид
    * @property  $data_sale Дата продажи
    * @property  $create_at Дата приема
    * @property int $client_id Клиент
    * @property string $repair_status Состояние ремонта
    * @property  $type_work Тип работы
    * @property  $sum Стоимость ремонта
    * @property int $injener_id Инженер
    * @property string $kontakty Контакты
    * @property string $fault Неисправность
    * @property  $photo Фото
    * @property int $user_id Пользователь
    * @property string $number_sale Заказ-наряд продавца
    * @property string $adress_del Адрес доставки
    * @property string $comment Комментарий
    * @property int $service_id СЦ
    * @property string $comment_job Комментарий к выполненной работе
    * @property string $nomer_otpravleniya Номер отправления
    * @property string $zavodskoy_nomer заводской номер
    * @property int $zip ЗИП
    * @property  $prichina_vydachi_akta Причина выдачи акта
    * @property  $done_repair_date Дата завершения ремонта
*/
class Order extends \yii\db\ActiveRecord
{

        public $filePhoto;


    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'order';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['type_repair_id', 'nomenklatura_id', 'status_id', 'client_id', 'injener_id', 'user_id', 'service_id', 'zip'], 'integer'],
            [['asc', 'sn', 'komplektaciya', 'vneshniy_vid', 'data_sale', 'create_at', 'repair_status', 'kontakty', 'fault', 'number_sale', 'adress_del', 'comment', 'comment_job', 'nomer_otpravleniya', 'zavodskoy_nomer', 'prichina_vydachi_akta'], 'string'],
            [['type_work', 'photo', 'done_repair_date'], 'safe'],
            [['sum'], 'number'],
            [['sn'], 'unique'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'type_repair_id' => Yii::t('app', 'Тип'),
            'asc' => Yii::t('app', 'Заказ наряд'),
            'nomenklatura_id' => Yii::t('app', 'Номенклатура'),
            'sn' => Yii::t('app', 'Серийный номер'),
            'status_id' => Yii::t('app', 'Статус'),
            'komplektaciya' => Yii::t('app', 'Комплектация'),
            'vneshniy_vid' => Yii::t('app', 'Внешний вид'),
            'data_sale' => Yii::t('app', 'Дата продажи'),
            'create_at' => Yii::t('app', 'Дата приема'),
            'client_id' => Yii::t('app', 'Клиент'),
            'repair_status' => Yii::t('app', 'Состояние ремонта'),
            'type_work' => Yii::t('app', 'Тип работы'),
            'sum' => Yii::t('app', 'Стоимость ремонта'),
            'injener_id' => Yii::t('app', 'Инженер'),
            'kontakty' => Yii::t('app', 'Контакты'),
            'fault' => Yii::t('app', 'Неисправность'),
            'photo' => Yii::t('app', 'Фото'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'number_sale' => Yii::t('app', 'Заказ-наряд продавца'),
            'adress_del' => Yii::t('app', 'Адрес доставки'),
            'comment' => Yii::t('app', 'Комментарий'),
            'service_id' => Yii::t('app', 'СЦ'),
            'comment_job' => Yii::t('app', 'Комментарий к выполненной работе'),
            'nomer_otpravleniya' => Yii::t('app', 'Номер отправления'),
            'zavodskoy_nomer' => Yii::t('app', 'заводской номер'),
            'zip' => Yii::t('app', 'ЗИП'),
            'prichina_vydachi_akta' => Yii::t('app', 'Причина выдачи акта'),
            'done_repair_date' => Yii::t('app', 'Дата завершения ремонта'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {
        // $filePhoto = UploadedFile::getInstance($this, 'filePhoto');
        // if($filePhoto ){
        //     if(is_dir('uploads') == false){
        //         mkdir('uploads');
        //     }

        //     $path = "uploads/".Yii::$app->security->generateRandomString().'.'.$filePhoto->extension;

        //     $filePhoto->saveAs($path);
        //     $this->photo = $path;
        // }

        if(is_array($this->type_work)){
            $this->type_work = implode(',', $this->type_work);
    }


        if ($this->isNewRecord){
          $this->create_at = date('Y-m-d H:i:s');
       }        

        if(isset($_POST['file_file_path'])){
            $this->photo = json_decode($this->photo, true);
            $newfile = json_decode($_POST['file_file_path'], true);
            $this->photo = json_encode(\yii\helpers\ArrayHelper::merge($this->photo ? $this->photo : [], $newfile ? $newfile : []), JSON_UNESCAPED_UNICODE);
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {


    }


    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTypeRepair()
    {
        return $this->hasOne(TypeRepair::className(), ['id' => 'type_repair_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getNomenklatura()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nomenklatura_id']);
    }

    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    
    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getInjener()
    {
        return $this->hasOne(User::className(), ['id' => 'injener_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    
    
    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getZip()
    {
        return $this->hasOne(Spare::className(), ['id' => 'zip']);
    }

    
    public function printValues()
    {
        $attributes = $this->attributeLabels();

        $tags = [];

        foreach ($attributes as $attribute => $label)
        {
            $label = str_replace(' ', '-', $label);

            if($attribute == 'type_repair_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'typeRepair.name');
            }
            else if($attribute == 'nomenklatura_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'nomenklatura.name');
            }
            else if($attribute == 'status_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'status.name');
            }
            else if($attribute == 'data_sale') {
                $tags['{'.mb_strtolower($label).'}'] = Yii::$app->formatter->asDate($this->$attribute, 'php:d.m.Y');
            }
            else if($attribute == 'create_at') {
                $tags['{'.mb_strtolower($label).'}'] = Yii::$app->formatter->asDate($this->$attribute, 'php:d.m.Y');
            }
            else if($attribute == 'client_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'client.name');
            }
            else if($attribute == 'injener_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'user.name');
            }
            else if($attribute == 'user_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'user.name');
            }
            else if($attribute == 'service_id') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'service.name');
            }
            else if($attribute == 'zip') {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, 'zip.name');
            }
            else {
                $tags['{'.mb_strtolower($label).'}'] = ArrayHelper::getValue($this, $attribute);
            }

        }


        return $tags;
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['order_id' => 'id']);
    }

}