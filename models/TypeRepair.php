<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "type_repair".
*
    * @property string $name Наименование
    * @property  $sum Стоимость
*/
class TypeRepair extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'type_repair';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['sum'], 'number'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'sum' => Yii::t('app', 'Стоимость'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['type_repair_id' => 'id']);
    }
        

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['typ_id' => 'id']);
    }

}