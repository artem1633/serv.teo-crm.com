<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $order_type_repair_id Заявки Тип
 * @property integer $order_asc Заявки Заказ наряд
 * @property integer $order_nomenklatura_id Заявки Номенклатура
 * @property integer $order_sn Заявки Серийный номер
 * @property integer $order_status_id Заявки Статус
 * @property integer $order_komplektaciya Заявки Комплектация
 * @property integer $order_vneshniy_vid Заявки Внешний вид
 * @property integer $order_data_sale Заявки Дата продажи
 * @property integer $order_create_at Заявки Дата приема
 * @property integer $order_client_id Заявки Клиент
 * @property integer $order_repair_status Заявки Состояние ремонта
 * @property integer $order_type_work Заявки Тип работы
 * @property integer $order_sum Заявки Стоимость ремонта
 * @property integer $order_injener_id Заявки Инженер
 * @property integer $order_kontakty Заявки Контакты
 * @property integer $order_fault Заявки Неисправность
 * @property integer $order_photo Заявки Фото
 * @property integer $order_user_id Заявки Пользователь
 * @property integer $order_number_sale Заявки Заказ-наряд продавца
 * @property integer $order_adress_del Заявки Адрес доставки
 * @property integer $order_comment Заявки Комментарий
 * @property integer $order_service_id Заявки СЦ
 * @property integer $order_comment_job Заявки Комментарий к выполненной работе
 * @property integer $order_nomer_otpravleniya Заявки Номер отправления
 * @property integer $order_zavodskoy_nomer Заявки заводской номер
 * @property integer $order_zip Заявки ЗИП
 * @property integer $order_prichina_vydachi_akta Заявки Причина выдачи акта
 * @property integer $job_typ_id Работы Тип
 * @property integer $job_sum Работы Стоимость
 * @property integer $job_replace_id Работы Заменено
 * @property integer $job_spare_id Работы Запчасть остается
 * @property integer $job_spare_old_id Работы Запчасть утиль
 * @property integer $job_order_id Работы Заявка
 * @property integer $deliver_nomenklatura_id Поставки Номенклатура
 * @property integer $deliver_origin Поставки Оригинал
 * @property integer $deliver_count Поставки Кол-во
 * @property integer $deliver_sn Поставки Серийник
 * @property integer $deliver_meriynik Поставки Мерийник
 * @property integer $deliver_garantiya Поставки Гарантия
 * @property integer $deliver_partiya Поставки Партия
 * @property integer $deliver_create_at Поставки Создан
 * @property integer $deliver_who_id Поставки Кто создал
 * @property integer $deliver_sc_id Поставки СЦ
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class ReportColumn extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_column';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_type_repair_id', 'order_asc', 'order_nomenklatura_id', 'order_sn', 'order_status_id', 'order_komplektaciya', 'order_vneshniy_vid', 'order_data_sale', 'order_create_at', 'order_client_id', 'order_repair_status', 'order_type_work', 'order_sum', 'order_injener_id', 'order_kontakty', 'order_fault', 'order_photo', 'order_user_id', 'order_number_sale', 'order_adress_del', 'order_comment', 'order_service_id', 'order_comment_job', 'order_nomer_otpravleniya', 'order_zavodskoy_nomer', 'order_zip', 'order_prichina_vydachi_akta', 'job_typ_id', 'job_sum', 'job_replace_id', 'job_spare_id', 'job_spare_old_id', 'job_order_id', 'deliver_nomenklatura_id', 'deliver_origin', 'deliver_count', 'deliver_sn', 'deliver_meriynik', 'deliver_garantiya', 'deliver_partiya', 'deliver_create_at', 'deliver_who_id', 'deliver_sc_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'order_type_repair_id' => 'Тип',
            'order_asc' => 'Заказ наряд',
            'order_nomenklatura_id' => 'Номенклатура',
            'order_sn' => 'Серийный номер',
            'order_status_id' => 'Статус',
            'order_komplektaciya' => 'Комплектация',
            'order_vneshniy_vid' => 'Внешний вид',
            'order_data_sale' => 'Дата продажи',
            'order_create_at' => 'Дата приема',
            'order_client_id' => 'Клиент',
            'order_repair_status' => 'Состояние ремонта',
            'order_type_work' => 'Тип работы',
            'order_sum' => 'Стоимость ремонта',
            'order_injener_id' => 'Инженер',
            'order_kontakty' => 'Контакты',
            'order_fault' => 'Неисправность',
            'order_photo' => 'Фото',
            'order_user_id' => 'Пользователь',
            'order_number_sale' => 'Заказ-наряд продавца',
            'order_adress_del' => 'Адрес доставки',
            'order_comment' => 'Комментарий',
            'order_service_id' => 'СЦ',
            'order_comment_job' => 'Комментарий к выполненной работе',
            'order_nomer_otpravleniya' => 'Номер отправления',
            'order_zavodskoy_nomer' => 'заводской номер',
            'order_zip' => 'ЗИП',
            'order_prichina_vydachi_akta' => 'Причина выдачи акта',
            'job_typ_id' => 'Тип',
            'job_sum' => 'Стоимость',
            'job_replace_id' => 'Заменено',
            'job_spare_id' => 'Запчасть остается',
            'job_spare_old_id' => 'Запчасть утиль',
            'job_order_id' => 'Заявка',
            'deliver_nomenklatura_id' => 'Номенклатура',
            'deliver_origin' => 'Оригинал',
            'deliver_count' => 'Кол-во',
            'deliver_sn' => 'Серийник',
            'deliver_meriynik' => 'Мерийник',
            'deliver_garantiya' => 'Гарантия',
            'deliver_partiya' => 'Партия',
            'deliver_create_at' => 'Создан',
            'deliver_who_id' => 'Кто создал',
            'deliver_sc_id' => 'СЦ',
            
        ];
    }


    /**
     * @return array
     */
    public function getColumnsAttributes(){
        return [
     'order_type_repair_id',
     'order_asc',
     'order_nomenklatura_id',
     'order_sn',
     'order_status_id',
     'order_komplektaciya',
     'order_vneshniy_vid',
     'order_data_sale',
     'order_create_at',
     'order_client_id',
     'order_repair_status',
     'order_type_work',
     'order_sum',
     'order_injener_id',
     'order_kontakty',
     'order_fault',
     'order_photo',
     'order_user_id',
     'order_number_sale',
     'order_adress_del',
     'order_comment',
     'order_service_id',
     'order_comment_job',
     'order_nomer_otpravleniya',
     'order_zavodskoy_nomer',
     'order_zip',
     'order_prichina_vydachi_akta',
     'job_typ_id',
     'job_sum',
     'job_replace_id',
     'job_spare_id',
     'job_spare_old_id',
     'job_order_id',
     'deliver_nomenklatura_id',
     'deliver_origin',
     'deliver_count',
     'deliver_sn',
     'deliver_meriynik',
     'deliver_garantiya',
     'deliver_partiya',
     'deliver_create_at',
     'deliver_who_id',
     'deliver_sc_id',
    
        ];
    }


    /**
     * @inheritdoc
     */
    public function getColumns()
    {
        $columns = [];


        foreach ($this->getColumnsAttributes() as $attr){
            if($this->$attr != null){
                $arr = explode('_', $attr);
  
                if($arr[0] == 'order'){
                    $str = 'order_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Заявки</span>';
                }
  
                if($arr[0] == 'job'){
                    $str = 'job_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Работы</span>';
                }
  
                if($arr[0] == 'deliver'){
                    $str = 'deliver_'.implode('_', array_slice($arr, 1));
                    $header = '<span class="label label-primary">Поставки</span>';
                }
  
                $columns[] = [
                    'attribute' => $str,
                    'header' => "<div style='width: 100%; text-align: center;'>{$header}</div>".$this->getAttributeLabel($attr),
                ];
            }
        }

        return $columns;
    }

}
