<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deliver;

/**
 * DeliverSearch represents the model behind the search form about `Deliver`.
 */
class DeliverSearch extends Deliver
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomenklatura_id', 'origin', 'count', 'garantiya', 'create_at', 'who_id', 'sc_id'], 'safe'],
            [['sn', 'meriynik', 'partiya'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Deliver::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'nomenklatura_id' => $this->nomenklatura_id,
            'who_id' => $this->who_id,
            'sc_id' => $this->sc_id,
        ]);

        $query->andFilterWhere(['like', 'count', $this->count])
            ->andFilterWhere(['like', 'sn', $this->sn])
            ->andFilterWhere(['like', 'meriynik', $this->meriynik])
            ->andFilterWhere(['like', 'garantiya', $this->garantiya])
            ->andFilterWhere(['like', 'partiya', $this->partiya]);

        return $dataProvider;
    }
}
