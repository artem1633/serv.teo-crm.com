<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "service".
*
    * @property string $name Наименование
    * @property string $city Город
    * @property string $adress  Адрес
    * @property string $responsible Ответственный
    * @property string $phone Телефон
    * @property string $post Должность
*/
class Service extends \yii\db\ActiveRecord
{

        

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'service';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'city', 'adress', 'responsible', 'phone', 'post'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'city' => Yii::t('app', 'Город'),
            'adress' => Yii::t('app', ' Адрес'),
            'responsible' => Yii::t('app', 'Ответственный'),
            'phone' => Yii::t('app', 'Телефон'),
            'post' => Yii::t('app', 'Должность'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['service_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getDelivers()
    {
        return $this->hasMany(Deliver::className(), ['sc_id' => 'id']);
    }

}