<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Job;

/**
 * JobSearch represents the model behind the search form about `Job`.
 */
class JobSearch extends Job
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typ_id', 'sum', 'spare_id', 'spare_old_id', 'order_id'], 'safe'],
            [['replace_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Job::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'typ_id' => $this->typ_id,
            'spare_id' => $this->spare_id,
            'spare_old_id' => $this->spare_old_id,
            'order_id' => $this->order_id,
        ]);

        $query->andFilterWhere(['like', 'sum', $this->sum])
            ->andFilterWhere(['like', 'replace_id', $this->replace_id]);

        return $dataProvider;
    }
}
