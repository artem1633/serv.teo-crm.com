<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "nomenclature".
*
    * @property string $name Наименование
    * @property string $code Артикул
*/
class Nomenclature extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'nomenclature';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['name', 'code', 'category'], 'string'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'code' => Yii::t('app', 'Артикул'),
            'category' => Yii::t('app', 'Категория'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['nomenklatura_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getSpares()
    {
        return $this->hasMany(Spare::className(), ['nomenklatura' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getDelivers()
    {
        return $this->hasMany(Deliver::className(), ['nomenklatura_id' => 'id']);
    }

}