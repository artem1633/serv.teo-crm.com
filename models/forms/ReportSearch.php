<?php 
namespace app\models\forms;


use app\models\Order; 
use app\models\Job; 
use app\models\Deliver; 
  

use app\models\ReportColumn;
use app\components\ComponentReport;
use Codeception\PHPUnit\ResultPrinter\Report;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class ReportSearch
 * @package app\models\forms
 */
class ReportSearch extends Model
{
    /**
     * @var array
     */
    public $setting;
    public $oldSetting;
    public $order_type_repair_id;
    public $order_asc;
    public $order_nomenklatura_id;
    public $order_sn;
    public $order_status_id;
    public $order_komplektaciya;
    public $order_vneshniy_vid;
    public $order_data_sale;
    public $order_create_at;
    public $order_client_id;
    public $order_repair_status;
    public $order_type_work;
    public $order_sum;
    public $order_injener_id;
    public $order_kontakty;
    public $order_fault;
    public $order_photo;
    public $order_user_id;
    public $order_number_sale;
    public $order_adress_del;
    public $order_comment;
    public $order_service_id;
    public $order_comment_job;
    public $order_nomer_otpravleniya;
    public $order_zavodskoy_nomer;
    public $order_zip;
    public $order_prichina_vydachi_akta;
    public $job_typ_id;
    public $job_sum;
    public $job_replace_id;
    public $job_spare_id;
    public $job_spare_old_id;
    public $job_order_id;
    public $deliver_nomenklatura_id;
    public $deliver_origin;
    public $deliver_count;
    public $deliver_sn;
    public $deliver_meriynik;
    public $deliver_garantiya;
    public $deliver_partiya;
    public $deliver_create_at;
    public $deliver_who_id;
    public $deliver_sc_id;
  
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_type_repair_id','order_asc','order_nomenklatura_id','order_sn','order_status_id','order_komplektaciya','order_vneshniy_vid','order_data_sale','order_create_at','order_client_id','order_repair_status','order_type_work','order_sum','order_injener_id','order_kontakty','order_fault','order_photo','order_user_id','order_number_sale','order_adress_del','order_comment','order_service_id','order_comment_job','order_nomer_otpravleniya','order_zavodskoy_nomer','order_zip','order_prichina_vydachi_akta','job_typ_id','job_sum','job_replace_id','job_spare_id','job_spare_old_id','job_order_id','deliver_nomenklatura_id','deliver_origin','deliver_count','deliver_sn','deliver_meriynik','deliver_garantiya','deliver_partiya','deliver_create_at','deliver_who_id','deliver_sc_id', 
    ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting' => 'Колонки',
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->from('drivers');


        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
        ]);





        return $dataProvider;
    }

    public static function fieldLabels()
    {
        return [

        ];
    }

}