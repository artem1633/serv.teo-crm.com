<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "spare".
*
    * @property string $name Наименование
    * @property int $nomenklatura Номенклатура
*/
class Spare extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'spare';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['nomenklatura'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'nomenklatura' => Yii::t('app', 'Номенклатура'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
            
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getNomenklatura()
    {
        return $this->hasOne(Nomenclature::className(), ['id' => 'nomenklatura']);
    }

        

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['spare_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getJobs0()
    {
        return $this->hasMany(Job::className(), ['spare_old_id' => 'id']);
    }
    

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['zip' => 'id']);
    }

}