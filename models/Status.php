<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
* This is the model class for table "status".
*
    * @property string $name Наименование
    * @property  $info_admin Уведомление админу
    * @property  $info_master Уведомление мастеру
    * @property  $info_client Уведомление клиенту
    * @property  $text_admin Текст уведомления админу
    * @property  $text_master Текст уведомления мастеру
    * @property  $text_client Текст уведомления клиенту
    * @property  $is_archive Отправить в архив
    * @property  $is_write Списать со склада
*/
class Status extends \yii\db\ActiveRecord
{

    public $fileUploading;
    

    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'status';
    }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'text_admin', 'text_master', 'text_client'], 'string'],
            [['info_admin', 'info_master', 'info_client', 'is_archive', 'is_write'], 'integer'],
        ];
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Наименование'),
            'info_admin' => Yii::t('app', 'Уведомление админу'),
            'info_master' => Yii::t('app', 'Уведомление мастеру'),
            'info_client' => Yii::t('app', 'Уведомление клиенту'),
            'text_admin' => Yii::t('app', 'Текст уведомления админу'),
            'text_master' => Yii::t('app', 'Текст уведомления мастеру'),
            'text_client' => Yii::t('app', 'Текст уведомления клиенту'),
            'is_archive' => Yii::t('app', 'Отправить в архив'),
            'is_write' => Yii::t('app', 'Списать со склада'),
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function beforeSave($insert) {



        
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

    }


    
    
    
    
    
    
    
    
    
                                                                                                                                                                                                                

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'id']);
    }
                        
}