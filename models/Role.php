<?php 
namespace app\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property int $id
 * @property string $name Название
 * @property integer $order_create Заявки Создание
 * @property integer $order_update Заявки Изменение
 * @property integer $order_view Заявки Просмотр
 * @property integer $order_view_all Заявки Просмотр всех
 * @property integer $order_delete Заявки Удаление
 * @property integer $job_create Работы Создание
 * @property integer $job_update Работы Изменение
 * @property integer $job_view Работы Просмотр
 * @property integer $job_view_all Работы Просмотр всех
 * @property integer $job_delete Работы Удаление
 * @property integer $deliver_create Поставки Создание
 * @property integer $deliver_update Поставки Изменение
 * @property integer $deliver_view Поставки Просмотр
 * @property integer $deliver_view_all Поставки Просмотр всех
 * @property integer $deliver_delete Поставки Удаление
 * @property integer $books Справочники
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_create', 'order_update', 'order_view', 'order_view_all', 'order_delete', 'job_create', 'job_update', 'job_view', 'job_view_all', 'job_delete', 'deliver_create', 'deliver_update', 'deliver_view', 'deliver_view_all', 'deliver_delete', 'books'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Роль',
            'order_create' => 'Создание',
            'order_update' => 'Изменение',
            'order_view' => 'Просмотр',
            'order_view_all' => 'Просмотр всех',
            'order_delete' => 'Удаление',
            'job_create' => 'Создание',
            'job_update' => 'Изменение',
            'job_view' => 'Просмотр',
            'job_view_all' => 'Просмотр всех',
            'job_delete' => 'Удаление',
            'deliver_create' => 'Создание',
            'deliver_update' => 'Изменение',
            'deliver_view' => 'Просмотр',
            'deliver_view_all' => 'Просмотр всех',
            'deliver_delete' => 'Удаление',
            'books' => 'Справочники'
];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
